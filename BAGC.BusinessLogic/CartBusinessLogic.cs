﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using BAGC.Core.Contracts;
using BAGC.domain.Entities;
using BAGC.domain.Models;
using BAGC.domain.Models.SerializedLinq;
using BAGC.domain.UnitOfWorks;
using BAGC.Infrastructure.UnitOfWorks;

namespace BAGC.BusinessLogic
{
    public class CartBusinessLogic : ICartBusinessLogic
    {
        private readonly IUnitOfWork _unitOfWork;

        public CartBusinessLogic(string connString)
        {
            _unitOfWork = new UnitOfWork(connString);
        }

        public CartBusinessLogic(IUnitOfWork work)
        {
            _unitOfWork = work;
        }
        public List<CartModel> FindCartByFamilyId(int familyId)
        {
            return _unitOfWork.CartRepository.GetByFunction(new SerializedLinqQuery
            {
                ConstantExpression = new ConstantExpression
                {
                    ConditionalOperator = ConditionalOperator.Equal,
                    Type = SerializedType.Int32,
                    PropertyName = "FamilyID",
                    Value = familyId.ToString(CultureInfo.InvariantCulture)
                },
                PageIndex = 0,
                PageSize = 100,
                SortAscending = false,
                SortProperty = "DateCreated"
            }).Select(MapCart).ToList();
        }

        private CartModel MapCart(Cart c)
        {
            return
                new CartModel
                {
                    CartId = c.CartId,
                    Address = new AddressModel
                    {
                        AddressLine1 = c.Address1,
                        AddressLine2 = c.Address2,
                        AddressLine3 = c.Address3,
                        City = c.City,
                        StateOrProvince = c.State,
                        Email = c.EMail,
                        PhoneNumber = c.Phone,
                        ZipCode = c.Zip
                    },
                    AmountDue = c.AmountDue.GetValueOrDefault(0),
                    Year = c.Year,
                    Birthday = c.BirthDate.GetValueOrDefault(DateTime.UtcNow),
                    EventId = c.EventID,
                    FamilyId = c.FamilyID,
                    FamilyMemberId = c.FamilyMemberID,
                    DateCreated = c.DateCreated.GetValueOrDefault(DateTime.UtcNow),
                    FirstName = c.FirstName,
                    RateTypeId = c.RateTypeID.GetValueOrDefault(0),
                    LastName = c.LastName
                };
        }

        private Cart MapCart(CartModel model)
        {
            return
                new Cart
                {
                    CartId = model.CartId,
                    Zip = model.Address.ZipCode,
                    State = model.Address.StateOrProvince,
                    Year = model.Year,
                    Address1 = model.Address.AddressLine1,
                    Address2 = model.Address.AddressLine2,
                    Address3 = model.Address.AddressLine3,
                    City = model.Address.City,
                    RateTypeID = model.RateTypeId,
                    AmountDue = model.AmountDue,
                    BirthDate = model.Birthday,
                    DateCreated = model.DateCreated,
                    EMail = model.Address.Email,
                    FamilyMemberID = model.FamilyMemberId,
                    FamilyID = model.FamilyId,
                    EventID = model.EventId,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    Phone = model.Address.PhoneNumber                   
                };
        }
        public CartModel FindCartByCartId(int cartId)
        {
            return
                MapCart((Cart) _unitOfWork.CartRepository.Find(cartId));
        }

        public void AddNewCart(CartModel cartModel)
        {
            cartModel.Validate();
            _unitOfWork.CartRepository.Add(MapCart(cartModel));
            _unitOfWork.Save();
        }

        public void DeleteCart(int id)
        {
            var cart  = (Cart)_unitOfWork.CartRepository.Find(id);
            _unitOfWork.CartRepository.Delete(cart);
        }

        public void EditCart(CartModel cartModel)
        {
            cartModel.Validate();
            _unitOfWork.CartRepository.Update(MapCart(cartModel));
            _unitOfWork.Save();
        }
    }
}
