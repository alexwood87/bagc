﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using BAGC.Core.Contracts;
using BAGC.domain.Entities;
using BAGC.domain.Models;
using BAGC.domain.Models.SerializedLinq;
using BAGC.domain.UnitOfWorks;
using BAGC.Infrastructure.UnitOfWorks;

namespace BAGC.BusinessLogic
{
    public class EventBusinessLogic : IEventBusinessLogic
    {
        private readonly IUnitOfWork _unitOfWork;
        
        public EventBusinessLogic(string connString)
        {
            _unitOfWork = new UnitOfWork(connString);
        }

        public EventBusinessLogic(IUnitOfWork work)
        {
            _unitOfWork = work;
        }
        public List<EventModel> SearchEvents(SerializedLinqQuery query)
        {
            return
                _unitOfWork.EventRepository.GetByFunction(query).Select(MapEvent).ToList();
        }

        private EventModel MapEvent(Event ev)
        {
            return
                new EventModel
                {
                    Description = ev.EventDesc,
                    EventName = ev.EventDesc,
                    Discount = ev.Discount.GetValueOrDefault(false),
                    EventId = ev.EventID,
                    IsMultiPartEvent = ev.MultiEvent.GetValueOrDefault(false),
                    EventDate = ev.EventDate

                };
        }

        public EventModel FindEventById(int eventId)
        {
            var model=
                MapEvent((Event)
                _unitOfWork.EventRepository.Find(eventId));
            var rates = _unitOfWork.RateRepository.GetByFunction(new SerializedLinqQuery
            {
                ConstantExpression = new ConstantExpression
                {
                    ConditionalOperator = ConditionalOperator.Equal,
                    PropertyName = "EventID",
                    Value = model.EventId.ToString(CultureInfo.InvariantCulture),
                    Type = SerializedType.Int32
                },
                PageIndex = 0,
                PageSize = 100,
                SortAscending = true,
                SortProperty = "RateTypeID"
            });
           
            return  model;
        }

        private RateModel MapRate(Rate rate, int? eventId = null)
        {
            return
                new RateModel
                {
                    Comments =  rate.Comment,
                    DateCreated = rate.DateCreated.GetValueOrDefault(DateTime.MinValue),
                    DateUpdated = rate.DateUpdated.GetValueOrDefault(DateTime.MinValue),
                    Year = rate.Year,
                    IsChild = rate.Child.GetValueOrDefault(false),
                    EventId = eventId == null ? rate.EventID : eventId.Value,
                    RateType = rate.RateTypeID,
                    Amount = rate.Amount.GetValueOrDefault(0),
                    Discount = rate.DiscountAmount.GetValueOrDefault(0)
                };
        }
        private Event MapEvent(EventModel model)
        {
            return
                new Event
                {
                    EventDate = model.EventDate,
                    Discount = model.Discount,
                    MultiEvent = model.IsMultiPartEvent,
                    EventID = model.EventId,
                    EventDesc = model.Description
                };
        }
        public void Add(EventModel eventModel)
        {
            eventModel.Validate();
            var ev =_unitOfWork.EventRepository.Add(MapEvent(eventModel));
            _unitOfWork.Save();
           
        }

        private Rate MapRate(RateModel model, int? eventId)
        {
            return
                new Rate
                {
                    RateTypeID = model.RateType,
                    Amount = model.Amount < 0 ? 0 : model.Amount,
                    DiscountAmount = model.Discount < 0 ? 0 : model.Discount,
                    EventID = eventId == null ? model.EventId : eventId.Value,
                    Year = string.IsNullOrEmpty(model.Year) ? DateTime.Now.Year.ToString(CultureInfo.InvariantCulture) : model.Year,
                    DateCreated = model.DateCreated == DateTime.MinValue ? DateTime.UtcNow : model.DateCreated,
                    DateUpdated = model.DateUpdated == DateTime.MinValue ? DateTime.UtcNow : model.DateUpdated,
                    Child = model.IsChild,
                    Comment = model.Comments
                };
        }
        public void Edit(EventModel eventModel)
        {
            eventModel.Validate();
            _unitOfWork.EventRepository.Update(MapEvent(eventModel));
        
            _unitOfWork.Save();
        }

        public List<EventModel> SearchByDescription(string desc)
        {
            return _unitOfWork.EventRepository.SearchByDescription(desc).Select(MapEvent).ToList();
        }
    }
}
