﻿using System;
using System.Collections.Generic;
using System.Linq;
using BAGC.Core.Contracts;
using BAGC.domain.Entities;
using BAGC.domain.Models;
using BAGC.domain.Models.SerializedLinq;
using BAGC.domain.UnitOfWorks;
using BAGC.Infrastructure.UnitOfWorks;

namespace BAGC.BusinessLogic
{
    public class FamilyBusinessLogic : IFamilyBusinessLogic
    {
        private readonly IUnitOfWork _unitOfWork;
        public FamilyBusinessLogic(string connString)
        {
            _unitOfWork = new UnitOfWork(connString);
        }

        public FamilyBusinessLogic(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public RateModel FindRateById(int id)
        {
            return
              MapRateModel((Rate) _unitOfWork.RateRepository.Find(id));
        }

        public List<RateModel> SearchRates(SerializedLinqQuery query)
        {
            return
                _unitOfWork.RateRepository.GetByFunction(query).Select(MapRateModel).ToList();
        }

        private MembershipTypeModel MapMembershipType(MembershipType mt)
        {
            if(mt == null)
                return null;
            return
                new MembershipTypeModel
                {
                    MembershipTypeId = mt.MembershipTypeId,
                    Name = mt.Name,
                    Year = mt.Year
                };
        }
         
        private RateModel MapRateModel(Rate rate)
        {
            var memType = _unitOfWork.MembershipTypeRepository.FindLastestForRateType(rate.RateTypeID);
            return
                new RateModel
                {
                    Amount =  rate.Amount.GetValueOrDefault(0),
                    Year = rate.Year,
                    Comments = rate.Comment,
                    EventId = rate.EventID,
                    DateCreated = rate.DateCreated.GetValueOrDefault(DateTime.UtcNow),
                    DateUpdated = rate.DateUpdated.GetValueOrDefault(DateTime.UtcNow),
                    Discount = rate.DiscountAmount.GetValueOrDefault(0),
                    IsChild = rate.Child.GetValueOrDefault(false),
                    RateType = rate.RateTypeID,
                    MembershipType = memType == null ? null : MapMembershipType(memType),
                    MembershipTypeId = memType == null ?(int?) null : memType.MembershipTypeId
                };
        }
        public List<FamilyMemberRegistration> Search(SerializedLinqQuery searchQuery)
        {
            return
                _unitOfWork.FamilyRepository.Search(searchQuery).Select(MapFamilyRegistration).ToList();
        }

        public FamilyMemberModel FindFamilyMemberByFamilyMemberId(int id)
        {
            var fam = (FamilyMember) _unitOfWork.FamilyMemberRepository.Find(id);
            return
                new FamilyMemberModel
                {
                    Address = new AddressModel
                    {
                        AddressLine1 = fam.Address1,
                        AddressLine2 = fam.Address2,
                        AddressLine3 = fam.Address3,
                        City = fam.City,
                        StateOrProvince = fam.State,
                        Email = fam.EMail,
                        PhoneNumber = fam.Phone,
                        ZipCode = fam.Phone
                    },
                    DateCreated = fam.DateCreated.GetValueOrDefault(DateTime.UtcNow),
                    DateUpdate = fam.DateUpdated.GetValueOrDefault(DateTime.UtcNow),
                    FamilyId = fam.FamilyID,
                    Comments = fam.Comment,
                    FirstName = fam.FirstName,
                    LastName = fam.LastName,
                    FamilyMemberId = fam.FamilyMemberID,
                    Rate = null,
                    RateTypeId = 0
                };
        }

        private FamilyMemberRegistration MapFamilyRegistration(Family member)
        {
        
            return
                new FamilyMemberRegistration
                {

                    FamilyId = member.FamilyID,
                    FirstName = member.FirstName,
                    LastName = member.LastName,
                    Address = new AddressModel{
                        PhoneNumber = member.Phone,
                        StateOrProvince = member.State,
                        AddressLine3 = member.Address3,
                        AddressLine2 = member.Address2,
                        AddressLine1 = member.Address1,
                        City = member.City,
                        Email = member.EMail,
                    
                        ZipCode = member.Zip
                    },
                    Year = member.Registrations.Any()? member.Registrations.OrderByDescending(x => x.RegistrationID).First().Year : "",
                    AmountDue = member.Registrations.Any() ? (double) member.Registrations.OrderByDescending(x => x.RegistrationID).First().AmountDue.GetValueOrDefault(0) : 0,
                    RateTypeId = member.Registrations.Any() ? member.Registrations.OrderByDescending(x => x.RegistrationID).First().RateTypeID : 0
                };
        }

        private FamilyMemberRegistration MapFamilyMemberRegistration(FamilyMember member)
        {
            var memType =
                _unitOfWork.MembershipTypeRepository.FindLatestOverrideForFamilyMember(member.FamilyMemberID) ??
                _unitOfWork.MembershipTypeRepository.FindLastestForFamily(member.FamilyID);
            var cart = _unitOfWork.CartRepository.GetByFunction(new SerializedLinqQuery
            {
                ConstantExpression = new ConstantExpression
                {
                    PropertyName = "FamilyMemberID",
                    Type = SerializedType.Int32,
                    Value = member.FamilyMemberID.ToString(),
                    ConditionalOperator = ConditionalOperator.Equal
                },
                PageIndex = 0,
                PageSize = int.MaxValue,
                SortProperty = "CartId",
                SortAscending = false
            }).FirstOrDefault();
            
            return
                new FamilyMemberRegistration
                {
                    Address = new AddressModel{
                        AddressLine1 = member.Address1,
                        AddressLine2 = member.Address2,
                        AddressLine3 = member.Address3,
                        City = member.City,
                        Email = member.EMail,
                        PhoneNumber = member.Phone,
                        StateOrProvince = member.State,
                        ZipCode = member.Zip
                    },
                    AmountDue = cart != null ? (double) cart.AmountDue.GetValueOrDefault(0) : 0,
                    MembershipType = MapMembershipType(memType),
                    FirstName = member.FirstName,
                    EventId =  cart != null ? cart.EventID : 0,
                    FamilyId = member.FamilyID,
                     
                    FamilyMemberId = member.FamilyMemberID,
                    LastName = member.LastName,
                   
                    Year = 
                         cart != null ?  cart.Year  :"0",
                    RateTypeId =
                        cart != null ? cart.RateTypeID.GetValueOrDefault(0) : 0 
                    
                };
        }

        public List<FamilyMemberRegistration> FindFamily(int familyId)
        {
            return _unitOfWork.FamilyMemberRepository.GetByFunction(new SerializedLinqQuery
            {
                ConstantExpression = new ConstantExpression
                {
                    ConditionalOperator = ConditionalOperator.Equal,
                    Type = SerializedType.Int32,
                    Value = familyId.ToString(),
                    PropertyName = "FamilyID"
                },
                PageIndex = 0,
                PageSize = int.MaxValue,
                SortAscending = true,
                SortProperty = "LastName"
            }).Select(MapFamilyMemberRegistration).ToList();
            // ((Family) _unitOfWork.FamilyRepository.Find(familyId)).FamilyMembers.Select(MapFamilyMemberRegistration)
            //   .ToList();
        }

        public FamilyMemberRegistration FindById(int id)
        {
            return
                MapFamilyMemberRegistration((FamilyMember) _unitOfWork.FamilyMemberRepository.Find(id));
        }

        public FamilyMemberModel FindHeadOfHouseByFamilyId(int familyId)
        {
            var fam = (Family)_unitOfWork.FamilyRepository.Find(familyId);
            var famMem = fam.FamilyMembers.OrderBy(x => x.FamilyMemberID).First();
            return
                MapFamilyMemberToModel(famMem, fam);
        }

        public void EditHeadOfHouse(FamilyMemberModel model)
        {
            if (model.MembershipTypeId.HasValue && model.MembershipType == null)
            {
                model.MembershipType =
                    MapMembershipType((MembershipType)_unitOfWork.MembershipTypeRepository.Find(model.MembershipTypeId.Value));
            }
            model.Validate();
            var fam = (Family)_unitOfWork.FamilyRepository.Find(model.FamilyId);
            fam.DateUpdated = DateTime.UtcNow;
            fam.LastName = model.LastName;
            fam.FirstName = model.FirstName;
            fam.Phone = model.Address.PhoneNumber;
            fam.State = model.Address.StateOrProvince;
            fam.City = model.Address.City;
            fam.EMail = model.Address.Email;
            fam.Zip = model.Address.ZipCode;
            fam.Comment = model.Comments;
            _unitOfWork.FamilyRepository.Update(fam);
            _unitOfWork.Save();
            if (model.MembershipTypeId.HasValue)
            {
                _unitOfWork.MembershipTypeRepository.AddUpdateFamilyMembershipType(model.FamilyId, model.MembershipTypeId.Value, model.MembershipType.Year);
            }
            _unitOfWork.Save();
        }
        private FamilyMemberModel MapFamilyMemberToModel(FamilyMember fam, Family family)
        {
            if (fam == null)
            {
                return null;
            }
            var memberType = _unitOfWork.MembershipTypeRepository.FindLastestForFamily(fam.FamilyID);
            var overrideMem = _unitOfWork.MembershipTypeRepository.FindLatestOverrideForFamilyMember(fam.FamilyMemberID);
            if (overrideMem != null)
                memberType = overrideMem;
            return
                new FamilyMemberModel
                {
                    Address = new AddressModel
                    {
                        AddressLine1 = fam.Address1,
                        AddressLine2 = fam.Address2,
                        AddressLine3 = fam.Address3,
                        City = fam.City,
                        Email = fam.EMail,
                        PhoneNumber = fam.Phone,
                        StateOrProvince = fam.State.Trim(),
                        ZipCode = fam.Zip
                    },
                    Comments = fam.Comment,
                    DateCreated = fam.DateCreated.GetValueOrDefault(DateTime.UtcNow),
                    DateUpdate = fam.DateUpdated.GetValueOrDefault(DateTime.UtcNow),
                    FamilyId = fam.FamilyID,
                    FamilyMemberId = fam.FamilyMemberID,
                    FirstName = fam.FirstName,
                    LastName = fam.LastName,
                    MembershipTypeId = memberType == null ? (int?) null : memberType.MembershipTypeId,
                    MembershipType = MapMembershipType(memberType)
                };
        }
        private FamilyMember MapFamilyMember(FamilyMemberModel model)
        {
            return
                new FamilyMember
                {
                    Address1 = model.Address.AddressLine1,
                    Address2 = model.Address.AddressLine2,
                    Address3 = model.Address.AddressLine3,
                    City = model.Address.City,
                    Comment = model.Comments,
                    DateCreated = model.DateCreated,
                    DateUpdated = model.DateUpdate,
                    EMail = model.Address.Email,
                    FamilyID = model.FamilyId,
                    FamilyMemberID = model.FamilyMemberId,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    Phone = model.Address.PhoneNumber,
                    State = model.Address.StateOrProvince,
                    Zip = model.Address.ZipCode
                };

        }

        private Rate MapRate(RateModel model)
        {
            return new Rate
            {
                Amount = model.Amount,
                DiscountAmount = model.Discount,
                Child = model.IsChild,
                Comment = model.Comments,
                Year = model.Year,
                EventID = model.EventId,
                DateUpdated = model.DateUpdated,
                DateCreated = model.DateCreated,
                RateTypeID = model.RateType
            };
        }

        private MembershipType MapMembershipTypeFromModel(MembershipTypeModel model)
        {
            if (model == null)
                return null;
            return
                new MembershipType
                {
                    MembershipTypeId = model.MembershipTypeId,
                    Year = model.Year,
                    Name = model.Name

                };
        }
        public void EditRate(RateModel rateModel)
        {
            rateModel.Validate();
            if (rateModel.RateType <= 0)
            {
                AddRate(rateModel);
                return;
            }
            if (rateModel.MembershipTypeId.HasValue && rateModel.MembershipType == null)
            {
                rateModel.MembershipType = MapMembershipType((MembershipType)_unitOfWork.MembershipTypeRepository.Find(rateModel.MembershipTypeId));
            }
            var rate = (Rate) _unitOfWork.RateRepository.Find(rateModel.RateType);
            rate.EventID = rateModel.EventId;
            rate.Year = rateModel.Year;
            rate.DiscountAmount = rateModel.Discount;
            rate.Amount = rateModel.Amount;
            rate.Child = rateModel.IsChild;
            rate.Comment = rateModel.Comments;
            rate.DateCreated = rateModel.DateCreated;
            rate.DateUpdated = rateModel.DateUpdated;
            if (rateModel.MembershipType != null)
            {
                var mem = MapMembershipTypeFromModel(rateModel.MembershipType);
                _unitOfWork.MembershipTypeRepository.DeleteRateMembershipType(rate.RateTypeID,mem.MembershipTypeId, int.Parse(rate.Year));
                _unitOfWork.MembershipTypeRepository.AddUpdateRateMembershipType(rate.RateTypeID, mem.MembershipTypeId, int.Parse(rate.Year));
                _unitOfWork.Save();
            }
            _unitOfWork.RateRepository.Update(rate);
            _unitOfWork.Save();
        }
        public void AddRate(RateModel rateModel)
        {
            if (rateModel.RateType <= 0)
            {
               var rate = _unitOfWork.RateRepository.Add(MapRate(rateModel));

                _unitOfWork.Save();
                if (rateModel.MembershipTypeId.HasValue && rateModel.MembershipType == null)
                {
                    rateModel.MembershipType = MapMembershipType((MembershipType)_unitOfWork.MembershipTypeRepository.Find(rateModel.MembershipTypeId));
                }
                if (rateModel.MembershipType != null)
                {
                    var mem = MapMembershipTypeFromModel(rateModel.MembershipType);
                    _unitOfWork.MembershipTypeRepository.DeleteRateMembershipType(rate.RateTypeID, mem.MembershipTypeId, int.Parse(rate.Year));
                    _unitOfWork.MembershipTypeRepository.AddUpdateRateMembershipType(rate.RateTypeID, mem.MembershipTypeId, int.Parse(rate.Year));
                    _unitOfWork.Save();
                }
            }
        }
        public void AddFamilyMember(FamilyMemberModel model)
        {
            model.Validate();
            var familyMember = MapFamilyMember(model);
            if (model.FamilyMemberId > 0)
            {
                EditFamilyMember(model);
                return;
            }
            var famId = familyMember.FamilyID;
            if (familyMember.FamilyID <= 0)
            {
                var fam = _unitOfWork.FamilyRepository.AddFamily(new Family
                {
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    Address1 = model.Address.AddressLine1,
                    Address2 = model.Address.AddressLine2,
                    Address3 = model.Address.AddressLine3,
                    City = model.Address.City,
                    EMail = model.Address.Email,
                    State = model.Address.StateOrProvince,
                    Phone = model.Address.PhoneNumber,
                    Zip = model.Address.ZipCode,
                    Comment = model.Comments,
                    DateCreated = model.DateCreated,
                    DateUpdated = model.DateUpdate,
                    FamilyID = 0
                    
                });
                _unitOfWork.Save();
                famId = fam.FamilyID;
                if (model.MembershipTypeId.HasValue)
                {
                    fam.MembershipTypeToFamilies.Add(new MembershipTypeToFamily
                    {
                        DateCreated = DateTime.UtcNow,
                        MembershipTypeId = model.MembershipTypeId.Value,
                        FamilyId = famId
                    });
                    _unitOfWork.FamilyRepository.Update(fam);
                    _unitOfWork.Save();
                }
            }
            familyMember.FamilyID = famId;
            var famMem =_unitOfWork.FamilyMemberRepository.Add(familyMember);
            _unitOfWork.Save();
            if (model.MembershipType != null)
            {
                var mem = MapMembershipTypeFromModel(model.MembershipType);
                _unitOfWork.MembershipTypeRepository.DeleteFamilyMemberMembershipType(famMem.FamilyMemberID, mem.MembershipTypeId, model.Rate != null ? int.Parse(model.Rate.Year) : DateTime.UtcNow.Year);
                int dt = DateTime.UtcNow.Year;
                int? eventId = null;
                if (model.Rate != null)
                {
                    eventId = model.Rate.EventId;
                    dt = int.Parse(model.Rate.Year);
                }
               
                _unitOfWork.MembershipTypeRepository.AddUpdateFamilyMemberMembershipType(famMem.FamilyMemberID,  mem.MembershipTypeId, eventId, dt );
             
                _unitOfWork.Save();
                
            }
            if (model.Rate != null && model.RateTypeId > 0)
            {
                EditRate(model.Rate);
            }
            else if(model.Rate != null)
            {
                AddRate(model.Rate);
            }
            _unitOfWork.Save();
        }

        public void AddOrUpdateMembershipType(MembershipTypeModel model)
        {
            var memType = MapMembershipTypeFromModel(model);
            if (memType.MembershipTypeId > 0)
            {
                _unitOfWork.MembershipTypeRepository.Update(memType);
            }
            else
            {
                _unitOfWork.MembershipTypeRepository.Add(memType);
            }
            _unitOfWork.Save();
        }

       
        public List<MembershipTypeModel> LoadAllMembershipTypes()
        {
            return
                _unitOfWork.MembershipTypeRepository.GetAll().Select(MapMembershipType).ToList();
        }
        public void EditFamilyMember(FamilyMemberModel model)
        {
            model.Validate();
            var familyMember = MapFamilyMember(model);
            if (model.Rate != null)
            {
                EditRate(model.Rate);
            }
            if (familyMember.FamilyMemberID <= 0)
            {
                AddFamilyMember(model);
                return;               
            }
            if (familyMember.MembershipTypeToFamilyMemberOverrides.Any())
            {
                familyMember.MembershipTypeToFamilyMemberOverrides.Clear();
                if (model.MembershipTypeId.HasValue)
                {
                    
                    familyMember.MembershipTypeToFamilyMemberOverrides.Add(new MembershipTypeToFamilyMemberOverride
                    {
                        MembershipTypeId = model.MembershipTypeId.Value,
                        EventId = model.Rate == null ? (int?)null : model.Rate.EventId,
                        FamilyMemberId = model.FamilyMemberId
                    });
                }
            }
            familyMember.EMail = familyMember.EMail.Trim();
            familyMember.Address1 = familyMember.Address1.Trim();
            familyMember.City = familyMember.City.Trim();
            familyMember.Phone = string.IsNullOrEmpty(familyMember.Phone) ? "" : familyMember.Phone.Trim();
            familyMember.Zip = string.IsNullOrEmpty(familyMember.Zip) ? "" : familyMember.Zip.Trim();
            familyMember.State = string.IsNullOrEmpty(familyMember.State) ? "" : familyMember.State.Trim();
            _unitOfWork.FamilyMemberRepository.Update(familyMember);
            _unitOfWork.Save();
             
        }
    }
}
