﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Transactions;
using BAGC.Core.Contracts;
using BAGC.domain.Entities;
using BAGC.domain.Models;
using BAGC.domain.Models.SerializedLinq;
using BAGC.domain.UnitOfWorks;
using BAGC.Infrastructure.UnitOfWorks;

namespace BAGC.BusinessLogic
{
    public class PaymentBusinessLogic : IPaymentBusinessLogic
    {
        private readonly IUnitOfWork _unitOfWork;
        public PaymentBusinessLogic(IUnitOfWork work)
        {
            _unitOfWork = work;
        }

        public PaymentBusinessLogic(string connString)
        {
            _unitOfWork = new UnitOfWork(connString);
        }
        public int MakePayment(PaymentModel paymentModel)
        {
            paymentModel.Validate();
            var res = _unitOfWork.PaymentRepository.Add(MapPayment(paymentModel));        
            _unitOfWork.Save();
           
            return res.PaymentID;
        }

        private Payment MapPayment(PaymentModel model)
        {
            return
                new Payment
                {
                    Year = model.Year,
                    EventID = model.EventId,
                    AmountPaid = model.AmountPaid,
                    PaymentDate = model.PaymentDate,
                    FamilyID = model.FamilyId,
                    Balance = model.RegistrationAmountDue - model.AmountPaid,
                    PaymentIdentifier = model.PaymentIdentifier,
                    Comment = model.Comments,
                    PaymentID = model.PaymentId,
                    PaymentMethod = model.PaymentMethod,
                    RegistrationAmountDue = model.RegistrationAmountDue,
                    FamilyMembershipDue = model.FamilyMembershipDue
                };
        }
        public List<PaymentModel> FindPaymentsByFamilyIdAndStartAndEndDate(int familyId, DateTime? startTime = null, DateTime? endDate = null)
        {
            var query = new SerializedLinqQuery
            {

                ConstantExpression = new ConstantExpression
                {
                    ConditionalOperator = ConditionalOperator.Equal,
                    Type = SerializedType.Int32,
                    PropertyName = "FamilyID",
                    Value = familyId.ToString(CultureInfo.InvariantCulture)
                },
                PageIndex = 0,
                PageSize = 100,
                SortProperty = "DateCreated",
                SortAscending = false
            };

            return
                _unitOfWork.PaymentRepository.GetByFunction(query).Where(x => x.PaymentDate >= startTime.GetValueOrDefault(DateTime.MinValue) && x.PaymentDate <= endDate.GetValueOrDefault(DateTime.MaxValue)).Select(MapPayment).ToList();
        }

        private PaymentModel MapPayment(Payment payment)
        {
            return
                new PaymentModel
                {
                    AmountPaid = payment.AmountPaid,
                    RegistrationAmountDue = payment.RegistrationAmountDue,
                    Comments = payment.Comment,
                    Year = payment.Year,
                    EventId = payment.EventID,
                    FamilyId = payment.FamilyID,
                    PaymentId = payment.PaymentID,
                    PaymentDate = payment.PaymentDate,
                    RateTypeId = payment.Registrations.OrderBy(x => x.DateCreated).First().RateTypeID,
                    PaymentIdentifier = payment.PaymentIdentifier,
                    FamilyMembershipDue = payment.FamilyMembershipDue,
                    PaymentMethod = payment.PaymentMethod
                };
        }
        public void DeletePaymentAndRegistrations(int paymentId)
        {
            using (var trans = new CommittableTransaction(new TimeSpan(1,0,0)))
            {
                try
                {
                   
                    _unitOfWork.PaymentRepository.Delete(paymentId);
                    _unitOfWork.Save();
                    trans.Commit();
                }
                catch (Exception ex)
                {
                    trans.Rollback(ex);
                    throw ex;
                }
            }
        }
    }
}
