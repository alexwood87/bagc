﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Transactions;
using BAGC.Core.Contracts;
using BAGC.domain.Entities;
using BAGC.domain.Models;
using BAGC.domain.Models.SerializedLinq;
using BAGC.domain.UnitOfWorks;
using BAGC.Infrastructure.UnitOfWorks;

namespace BAGC.BusinessLogic
{
    public class RegistrationBusinessLogic : IRegistrationBusinessLogic
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IPaymentBusinessLogic _paymentBusinessLogic;
        public RegistrationBusinessLogic(string connString)
        {
            _unitOfWork = new UnitOfWork(connString);
            _paymentBusinessLogic = new PaymentBusinessLogic(_unitOfWork);
        }

        public RegistrationBusinessLogic(IUnitOfWork work, IPaymentBusinessLogic payLogic)
        {
            _unitOfWork = work;
            _paymentBusinessLogic = payLogic;
        }
        public void Register(RegistrationModel registrationModel)
        {
            if (registrationModel.Payment.PaymentId <= 0)
            {
               registrationModel.Payment.PaymentId =  _paymentBusinessLogic.MakePayment(registrationModel.Payment);
            }
            _unitOfWork.RegistrationRepository.Add(MapRegistration(registrationModel));
        }
        
        private Registration MapRegistration(RegistrationModel model)
        {
            var person = (FamilyMember) _unitOfWork.FamilyMemberRepository.Find(model.FamilyMemberId);
            return
                new Registration
                {
                    Address1 = model.Address.AddressLine1,
                    Year = model.Year,
                    RegistrationID = model.RegistrationId,
                    FirstName  = person.FirstName,
                    FamilyID = person.FamilyID,
                    FamilyMemberID = person.FamilyMemberID,
                    EventID = model.EventId,
                    DateUpdated = model.DateUpdated,
                    DateCreated = model.DateCreated,
                    EMail = model.Address.Email,
                    Prepaid = model.Prepaid,
                    Attending = model.Attending,
                    AmountDue = model.Payment.RegistrationAmountDue,
                    Phone = model.Address.PhoneNumber,
                    Address2 = model.Address.AddressLine2,
                    Address3 = model.Address.AddressLine3,
                    City = model.Address.City,
                    LastName = person.LastName,
                    State = model.Address.StateOrProvince,
                    Zip = model.Address.ZipCode,
                    Comment = model.Comments,
                    PaymentID = model.Payment != null ? model.Payment.PaymentId : (int?)null,
                    RateTypeID = model.Payment.RateTypeId 
                };
        }
        
        public RegistrationModel FindByFamilyMemberIdAndEventId(int? familyMemberId, int? eventId)
        {
            var query = new SerializedLinqQuery();
            if (familyMemberId != null && eventId != null)
            {
                query.CompositeExpression = new CompositeExpression
                {
                    LeftExpression = new ConstantExpression
                    {
                        ConditionalOperator = ConditionalOperator.Equal,
                        Type = SerializedType.Int32,
                        PropertyName = "FamilyMemberID",
                        Value = familyMemberId.Value.ToString(CultureInfo.InvariantCulture)
                    },
                    LogicOperator = LogicOperator.AND,
                    RightExpression = new ConstantExpression
                    {
                        ConditionalOperator = ConditionalOperator.Equal,
                        Type = SerializedType.Int32,
                        PropertyName = "EventID",
                        Value = eventId.Value.ToString(CultureInfo.InvariantCulture)
                    }
                };
            }
            else if(familyMemberId != null)
            {
                query.ConstantExpression = new ConstantExpression
                {
                    ConditionalOperator = ConditionalOperator.Equal,
                    Type = SerializedType.Int32,
                    PropertyName = "FamilyMemberID",
                    Value = familyMemberId.Value.ToString(CultureInfo.InvariantCulture)
                };
            }
            else if (eventId != null)
            {
                query.ConstantExpression = new ConstantExpression
                {
                    ConditionalOperator = ConditionalOperator.Equal,
                    Type = SerializedType.Int32,
                    PropertyName = "EventID",
                    Value = eventId.Value.ToString(CultureInfo.InvariantCulture)
                };
            }
            query.PageIndex = 0;
            query.PageSize = 100;
            query.SortAscending = false;
            query.SortProperty = "DateCreated";
            var registrations = _unitOfWork.RegistrationRepository.GetByFunction(query);
            return registrations.Select(x => new RegistrationModel
            {
                Year = x.Year,
                Address = new AddressModel
                {
                    AddressLine1 = x.Address1,
                    AddressLine2 = x.Address2,
                    AddressLine3 = x.Address3,
                    City = x.City,
                    StateOrProvince = x.State,
                    Email = x.EMail,
                    PhoneNumber = x.Phone,
                    ZipCode = x.Zip
                },
                Attending = x.Attending.GetValueOrDefault(false),
                EventId = x.EventID,
                DateCreated = x.DateCreated.GetValueOrDefault(DateTime.UtcNow),
                FamilyId = x.FamilyID,
                FamilyMemberId = x.FamilyMemberID,
                Comments = x.Comment,
                DateUpdated = x.DateUpdated.GetValueOrDefault(DateTime.UtcNow),
                Prepaid = x.Prepaid.GetValueOrDefault(false),
                Payment = new PaymentModel
                {
                    AmountPaid = x.Payment.AmountPaid,
                    Comments = x.Payment.Comment,
                    Year = x.Payment.Year,
                    EventId = x.EventID,
                    FamilyId = x.FamilyID,
                    PaymentDate = x.Payment.PaymentDate,
                    PaymentId = x.PaymentID.GetValueOrDefault(0) ,
                    RateTypeId = x.RateTypeID,
                    PaymentIdentifier = x.Payment.PaymentIdentifier,
                    RegistrationAmountDue = x.Payment.RegistrationAmountDue,
                    PaymentMethod = x.Payment.PaymentMethod
                },
                RegistrationId = x.RegistrationID
            }).FirstOrDefault();
        }

        public void DeleteRegstration(int registrationId)
        {
            using (var trans = new CommittableTransaction(new TimeSpan(1, 0, 0)))
            {
                try
                {
                    _unitOfWork.RegistrationRepository.Delete(registrationId);
                    _unitOfWork.Save();
                    trans.Commit();
                }
                catch (Exception ex)
                {
                    trans.Rollback(ex);
                }
            }
        }

        private RegistrationModel MapRegistrationToModel(Registration reg)
        {
            return
                new RegistrationModel
                {
                    Address = new AddressModel
                    {
                        AddressLine1 = reg.Address1,
                        AddressLine2 = reg.Address2,
                        AddressLine3 = reg.Address3,
                        City = reg.City,
                        StateOrProvince = reg.State,
                        Email = reg.EMail,
                        PhoneNumber = reg.Phone,
                        ZipCode = reg.Zip
                    },
                    Attending = reg.Attending.GetValueOrDefault(false),
                    Comments = reg.Comment,
                    DateCreated = reg.DateCreated.GetValueOrDefault(DateTime.UtcNow),
                    DateUpdated = reg.DateUpdated.GetValueOrDefault(DateTime.UtcNow),
                    EventId = reg.EventID,
                    FamilyId = reg.FamilyID,
                    FamilyMemberId = reg.FamilyMemberID,
                    Payment = reg.PaymentID != 0
                        ? new PaymentModel
                        {
                            AmountPaid = reg.Payment.AmountPaid,
                            Comments = reg.Payment.Comment,
                            EventId = reg.EventID,
                            FamilyId = reg.Payment.FamilyID,
                            FamilyMembershipDue = reg.Payment.FamilyMembershipDue,
                            RateTypeId = reg.RateTypeID,
                            PaymentDate = reg.Payment.PaymentDate,
                            PaymentId = reg.Payment.PaymentID,
                            PaymentIdentifier = reg.Payment.PaymentIdentifier,
                            PaymentMethod = reg.Payment.PaymentMethod,
                            RegistrationAmountDue = reg.Payment.RegistrationAmountDue,
                            Year = reg.Payment.Year
                        }
                        : null,
                        Year = reg.Year,
                        Prepaid = reg.Prepaid.GetValueOrDefault(false),
                        RegistrationId = reg.RegistrationID
                };
        }
        public List<RegistrationModel> SearchRegistration(SerializedLinqQuery query)
        {
            return
                _unitOfWork.RegistrationRepository.GetByFunction(query).Select(MapRegistrationToModel).ToList();
        }
    }
}
