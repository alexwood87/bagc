﻿using System;

namespace BAGC.Common
{
    public static class Extensions
    {
        public static object GetPublicProperty(this object obj, string name)
        {
            try
            {
                return obj.GetType().GetProperty(name).GetValue(obj);
            }
            catch (Exception ex)
            {
                return null;
            }
        } 
    }
}
