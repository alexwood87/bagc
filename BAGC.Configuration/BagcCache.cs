﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.Caching;
using BAGC.BusinessLogic;
using BAGC.Core.Contracts;
using BAGC.domain.Models;

namespace BAGC.Configuration
{
    public class BagcCache
    {
        private static readonly MemoryCache Cache = MemoryCache.Default;
        private static volatile bool _loaded = false;
        private static readonly IEventBusinessLogic EventBusinessLogic =
            new EventBusinessLogic(ConfigurationManager.ConnectionStrings["BAGCConnString"].ConnectionString);

        public static List<EventModel> GetAllEvents()
        {
            return ((List<EventModel>) Cache["Events"]);
        }

        public static void Init()
        {
            if (_loaded)
            {
                return;
            }
            _loaded = true;
            Cache.Set(new CacheItem("Events", EventBusinessLogic.SearchEvents(null)), new CacheItemPolicy
            {
                UpdateCallback = delegate
                {
                    Cache["Events"] = EventBusinessLogic.SearchEvents(null);
                },
                SlidingExpiration = new TimeSpan(0,30,0)
            });
        }
    }
}
