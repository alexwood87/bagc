﻿using System.Collections.Generic;
using BAGC.domain.Models;

namespace BAGC.Core.Contracts
{
    public interface ICartBusinessLogic
    {
        List<CartModel> FindCartByFamilyId(int familyId);
        CartModel FindCartByCartId(int cartId);
        void AddNewCart(CartModel cartModel);
        void EditCart(CartModel cartModel);
        void DeleteCart(int id);
    }
}
