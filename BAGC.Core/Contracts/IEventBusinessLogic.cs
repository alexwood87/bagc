﻿using System.Collections.Generic;
using BAGC.domain.Models;
using BAGC.domain.Models.SerializedLinq;

namespace BAGC.Core.Contracts
{
    public interface IEventBusinessLogic
    {
        List<EventModel> SearchEvents(SerializedLinqQuery query);
        EventModel FindEventById(int eventId);
        void Add(EventModel eventModel);
        void Edit(EventModel eventModel);
        List<EventModel> SearchByDescription(string desc);
    }
}
