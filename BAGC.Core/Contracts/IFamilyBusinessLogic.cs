﻿using System.Collections.Generic;
using BAGC.domain.Models;
using BAGC.domain.Models.SerializedLinq;

namespace BAGC.Core.Contracts
{
    public interface IFamilyBusinessLogic
    {
        List<FamilyMemberRegistration> Search(SerializedLinqQuery searchQuery);
        List<FamilyMemberRegistration> FindFamily(int familyId);
        FamilyMemberRegistration FindById(int id);
        void AddFamilyMember(FamilyMemberModel model);
        void EditFamilyMember(FamilyMemberModel model);
        void EditRate(RateModel rateModel);
        void AddRate(RateModel rateModel);
        List<RateModel> SearchRates(SerializedLinqQuery query);
        RateModel FindRateById(int id);
        FamilyMemberModel FindFamilyMemberByFamilyMemberId(int id);
        List<MembershipTypeModel> LoadAllMembershipTypes();
        void AddOrUpdateMembershipType(MembershipTypeModel model);
        FamilyMemberModel FindHeadOfHouseByFamilyId(int familyId);
        void EditHeadOfHouse(FamilyMemberModel model);
    }
}
