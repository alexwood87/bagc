﻿using System;
using System.Collections.Generic;
using BAGC.domain.Models;

namespace BAGC.Core.Contracts
{
    public interface IPaymentBusinessLogic
    {
        int MakePayment(PaymentModel paymentModel);

        List<PaymentModel> FindPaymentsByFamilyIdAndStartAndEndDate(int familyId,
            DateTime? startTime = null, DateTime? endDate = null);

        void DeletePaymentAndRegistrations(int paymentId);
    }
}
