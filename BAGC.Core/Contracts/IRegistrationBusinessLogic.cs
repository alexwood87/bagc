﻿using System.Collections.Generic;
using BAGC.domain.Models;
using BAGC.domain.Models.SerializedLinq;

namespace BAGC.Core.Contracts
{
    public interface IRegistrationBusinessLogic
    {
        void Register(RegistrationModel registrationModel);
        RegistrationModel FindByFamilyMemberIdAndEventId(int? familyMemberId, int? eventId);
        List<RegistrationModel> SearchRegistration(SerializedLinqQuery query);
        void DeleteRegstration(int registrationId);
    }
}
