﻿using System;
using BAGC.Common;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using BAGC.domain.Models.SerializedLinq;
using ConstantExpression = BAGC.domain.Models.SerializedLinq.ConstantExpression;
using Expression = BAGC.domain.Models.SerializedLinq.Expression;

namespace BAGC.Infrastructure.Helpers
{
    public class LinqBuilder<TE> where TE : class
    {
        public IQueryable<TE> BuildExpression(IQueryable<TE> set, SerializedLinqQuery query)
        {
            var ex = query.ConstantExpression ?? (Expression)query.CompositeExpression;
            IQueryable<TE> res = set.AsQueryable();
            res = BuildSubExpression(res, ex);
            return res;

        }

        private IQueryable<TE> BuildSubExpression(IQueryable<TE> set, Expression exp)
        {
            var expConst = exp as ConstantExpression;
            if (expConst != null)
            {
                return
                    ConditionalComparison(set, expConst.PropertyName, Translate(expConst.Value, expConst.Type), expConst.ConditionalOperator);
            }
           
            else
            {
                var expComp = exp as CompositeExpression;
                var q1 = BuildSubExpression(set, expComp.LeftExpression);
                if (expComp.LogicOperator == LogicOperator.AND)
                {
                    q1 = BuildSubExpression(q1, expComp.RightExpression);
                }
                else if (expComp.LogicOperator == LogicOperator.OR)
                {
                    var q2 = BuildSubExpression(set, expComp.RightExpression);
                    q1 = q1.Union(q2);
                }
                else
                {
                    ConditionalOperator op;
                    switch (expComp.LeftExpression.ConditionalOperator)
                    {
                        case ConditionalOperator.NotEqual:
                            op = ConditionalOperator.Equal;
                            break;
                        case ConditionalOperator.GreaterThan:
                            op = ConditionalOperator.LessThanEqual;
                            break;
                        case ConditionalOperator.GreaterThanEqual:
                            op = ConditionalOperator.LessThan;
                            break;
                        case ConditionalOperator.LessThan:
                            op = ConditionalOperator.GreaterThanEqual;
                            break;
                        case ConditionalOperator.LessThanEqual:
                            op = ConditionalOperator.GreaterThan;
                            break;
                        case ConditionalOperator.Equal:
                            op = ConditionalOperator.NotEqual;
                            break;
                        default:
                            throw new NotImplementedException("Inverse Contains Not Implemented");
                    }
                    return
                        ConditionalComparison(set, expComp.LeftExpression.PropertyName, expComp.LeftExpression.Value, op);
                }
                return q1;
            }
        }

        private object Translate(string val, SerializedType t)
        {
            decimal d;
            int i;
            long l;
            double di;
            float f;
            DateTime dt;
            switch (t)
            {
                case SerializedType.Decimal:
                    return decimal.Parse(val);
                case SerializedType.DecimalNull:
                    return decimal.TryParse(val, out d) ? d : (decimal?)null;
                case SerializedType.Double:
                    return double.Parse(val);
                case SerializedType.DoubleNull:
                    return double.TryParse(val, out di) ? di : (double?) null;
                case SerializedType.Float:
                    return float.Parse(val); 
                case SerializedType.FloatNull:
                    return float.TryParse(val, out f) ? f : (float?) null;
                case SerializedType.Int32:
                    return int.Parse(val);
                case SerializedType.Int32Null:
                    return int.TryParse(val, out i) ? i : (int?) null;
                case SerializedType.Long:
                    return long.Parse(val);
                case SerializedType.LongNull:
                    return long.TryParse(val, out l) ? l : (long?) null;
                case SerializedType.DateTime:
                    return
                        DateTime.Parse(val);
                case SerializedType.DateTimeNull:
                    return DateTime.TryParse(val, out dt) ? dt : (DateTime?) null;
                default:
                    return val;
            }
        }
        private Type GetPropertyType(SerializedType t)
        {
            switch (t)
            {
                case SerializedType.Decimal:
                    return typeof (decimal);
                case SerializedType.DecimalNull:
                    return typeof (decimal?);
                case SerializedType.Double:
                    return typeof (double);
                case SerializedType.DoubleNull:
                    return typeof (double?);
                case SerializedType.Float:
                    return typeof (float);
                case SerializedType.FloatNull:
                    return typeof (float?);
                case SerializedType.Int32:
                    return typeof (int);
                case SerializedType.Int32Null:
                    return typeof (int?);
                case SerializedType.Long:
                    return typeof (long);
                case SerializedType.LongNull:
                    return typeof (long?);
                default:
                    return typeof (string);
            }    
        }

        private IQueryable<TE> ConditionalComparison(IQueryable<TE> set, string properyName, object value, ConditionalOperator oper)
        {
            switch (oper)
            {
                case ConditionalOperator.Equal:
                    set = set.ToList().Where(x => x.GetPublicProperty(properyName).ToString() == value.ToString()).AsQueryable();
                    break;
                case ConditionalOperator.GreaterThan:
                    set = set.ToList().Where(x => ((decimal) x.GetPublicProperty(properyName)) > ((decimal)value)).AsQueryable();
                    break; 
                case ConditionalOperator.GreaterThanEqual:
                    set = set.ToList().Where(x => ((decimal) x.GetPublicProperty(properyName)) >= ((decimal)value)).AsQueryable();
                    break; 
                case ConditionalOperator.LessThan:
                    set = set.ToList().Where(x => ((decimal) x.GetPublicProperty(properyName)) < ((decimal)value)).AsQueryable();
                    break; 
                case ConditionalOperator.LessThanEqual:
                    set = set.ToList().Where(x => ((decimal) x.GetPublicProperty(properyName)) <= ((decimal)value)).AsQueryable();
                    break; 
                case ConditionalOperator.NotEqual:
                {
                    set = set.ToList().Where(x => x.GetPublicProperty(properyName) != value).AsQueryable();
                    
                    break;
                }
                case ConditionalOperator.Contains:
                    set = set.ToList().Where(x => ((string) x.GetPublicProperty(properyName)).Contains( ((string)value))).AsQueryable();
                    break; 

            }
            return set;
        }
        //private IQueryable<TE> LogicalCompare(IQueryable<TE> set, System.Linq.Expressions.Expression left,
        //    System.Linq.Expressions.Expression right, LogicOperator oper)
        //{
        //    switch (oper)
        //    {
        //        case LogicOperator.AND:
        //            return System.Linq.Expressions.Expression.AndAlso(left, right);
        //        case LogicOperator.OR:
        //            return System.Linq.Expressions.Expression.OrElse(left, right);
        //        case LogicOperator.NOT:
        //            return System.Linq.Expressions.Expression.Not(left);
        //    }
        //    return null;
        //}
    }
}
