﻿using BAGC.domain.Entities;
using BAGC.domain.Repositories;

namespace BAGC.Infrastructure.Repositories
{
    public class CartRepository : GenericRepository<Cart>, ICartRepository
    {
        public CartRepository(BAGCEntities context) : base(context)
        {
        }

        public void Delete(Cart cart)
        {
            _context.Carts.Remove(cart);
            _context.SaveChanges();
        }
    }
}
