﻿using System.Collections.Generic;
using System.Linq;
using BAGC.domain.Entities;
using BAGC.domain.Models;
using BAGC.domain.Repositories;

namespace BAGC.Infrastructure.Repositories
{
    public class EventRepository : GenericRepository<Event>, IEventRepository
    {
        public EventRepository(BAGCEntities context) : base(context)
        {
        }

        public List<Event> SearchByDescription(string desc)
        {
            return
                _context.Events.Where(x => x.EventDesc.Contains(desc)).ToList();
        }

    }
}
