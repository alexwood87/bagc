﻿using BAGC.domain.Entities;
using BAGC.domain.Repositories;

namespace BAGC.Infrastructure.Repositories
{
    public class FamilyMemberRepository : GenericRepository<FamilyMember>, IFamilyMemberRepository
    {
        public FamilyMemberRepository(BAGCEntities context) : base(context)
        {
        }
    }
}
