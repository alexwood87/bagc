﻿using System.Collections.Generic;
using System.Linq;
using BAGC.domain.Entities;
using BAGC.domain.Models.SerializedLinq;
using BAGC.domain.Repositories;

namespace BAGC.Infrastructure.Repositories
{
    public class FamilyRepository : GenericRepository<Family>, IFamilyRepository
    {
        public FamilyRepository(BAGCEntities context) : base(context)
        {
       
        }

        public List<Family> Search(SerializedLinqQuery query)
        {
            return GetByFunction(query).Skip(query.PageSize * query.PageIndex).Take(query.PageSize).ToList();
        }

        public Family AddFamily(Family family)
        {
            family = _context.Families.Add(family);
            _context.SaveChanges();
            return family;
        }
    }
}
