﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using BAGC.Common;
using BAGC.domain.Entities;
using BAGC.domain.Models.SerializedLinq;
using BAGC.domain.Repositories;
using BAGC.Infrastructure.Helpers;

namespace BAGC.Infrastructure.Repositories
{
    public class GenericRepository<TE> : IGenericRepository<TE> where TE : class
    {
        protected readonly BAGCEntities _context;

        public GenericRepository(BAGCEntities context)
        {
            _context = context;
        }
        public object Find(object id)
        {
            return
                _context.Set<TE>().Find(id);
        }

        public void Save(TE entity)
        {
            throw new NotImplementedException();
        }

        public void Update(TE entity)
        {
            _context.Set<TE>().AddOrUpdate(entity);
            _context.SaveChanges();
        }

        public TE Add(TE entity)
        {
            var et = _context.Set<TE>().Add(entity);
            _context.SaveChanges();
            return et;
        }

        public List<TE> GetAll()
        {
            return _context.Set<TE>().ToList();
        }

        public List<TE> GetByFunction(SerializedLinqQuery query)
        {
            if (query == null || (query.CompositeExpression == null && query.ConstantExpression == null))
            {
                return _context.Set<TE>().ToList();
            }
            var linqBuilder = new LinqBuilder<TE>();
            IQueryable<TE> q = _context.Set<TE>().AsQueryable();
            var q3 = _context.Set<TE>().AsQueryable();
           
             q = linqBuilder.BuildExpression(q, query);
            
            var q2 =
                q.ToList();
             if (query.SortProperty != null)
             {
                 q2 = query.SortAscending ? q2.OrderBy(x => x.GetPublicProperty(query.SortProperty)).ToList() : q2.OrderByDescending(x => x.GetPublicProperty(query.SortProperty)).ToList();
             }
            return q2;
        }
    }
}
