﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BAGC.domain.Entities;
using BAGC.domain.Repositories;

namespace BAGC.Infrastructure.Repositories
{
    public class MembershipTypeRepository: GenericRepository<MembershipType>, IMembershipTypeRepository
    {
        public MembershipTypeRepository(BAGCEntities context) : base(context)
        {
        }

        public MembershipType FindLastestForFamily(int familyId)
        {
            var membFams = _context.MembershipTypeToFamilies.Where(x => x.FamilyId == familyId).ToList();
            var now = membFams.OrderByDescending(x => x.MembershipType.Year).FirstOrDefault();
            if(now == null) 
                return null;
            return now.MembershipType;
        }

        public MembershipType FindLatestOverrideForFamilyMember(int familyMemberId)
        {
            var famMemOverride =
                _context.MembershipTypeToFamilyMemberOverrides.Where(x => x.FamilyMemberId == familyMemberId);
            return
                famMemOverride.Any(x => x.MembershipType.Year == DateTime.UtcNow.Year)
                    ? famMemOverride.OrderByDescending(x => x.MembershipType.Year)
                        .First(x => x.MembershipType.Year == DateTime.UtcNow.Year)
                        .MembershipType
                    : null;
        }

        public MembershipType FindLastestForRateType(int rateTypeId)
        {
            var rateMems = _context.RateToMembershipTypes.Where(x => x.RateTypeId == rateTypeId);
            return rateMems.Any(x => x.MembershipType.Year == DateTime.UtcNow.Year)? 
                rateMems.OrderByDescending(x => x.MembershipType.Year)
                .First(x => x.MembershipType.Year == DateTime.UtcNow.Year).MembershipType : null;
        }

        public void AddUpdateRateMembershipType(int rateType, int membershipTypeId, int year)
        {
            if (_context.RateToMembershipTypes.Any(x => x.MembershipType.Year == year && x.MembershipTypeId == membershipTypeId && x.RateTypeId == rateType))
            {
                return;
            }
            
            _context.RateToMembershipTypes.Add(new RateToMembershipType
            {
                MembershipTypeId = membershipTypeId,
                DateCreated = DateTime.UtcNow,
                RateTypeId = rateType
            });

            _context.SaveChanges();
        }

        public void AddUpdateFamilyMemberMembershipType(int familyMemberId, int membershipTypeId, int? eventId, int year)
        {
            if (_context.MembershipTypeToFamilyMemberOverrides.Any(x => x.MembershipType.Year == year && (x.EventId == eventId) && x.MembershipTypeId == membershipTypeId && x.FamilyMemberId == familyMemberId))
            {
                return;
            }

            _context.MembershipTypeToFamilyMemberOverrides.Add(new MembershipTypeToFamilyMemberOverride
            {
                MembershipTypeId = membershipTypeId,
                FamilyMemberId =  familyMemberId,
                EventId = eventId               
            });

            _context.SaveChanges();
        }

        public void AddUpdateFamilyMembershipType(int familyId, int membershipTypeId, int year)
        {
            if (_context.MembershipTypeToFamilies.Any(x => x.MembershipType.Year == year && x.MembershipTypeId == membershipTypeId && x.FamilyId == familyId))
            {
                return;
            }

            _context.MembershipTypeToFamilies.Add(new MembershipTypeToFamily
            {
                MembershipTypeId = membershipTypeId,
                FamilyId = familyId,
                DateCreated = DateTime.UtcNow                
            });

            _context.SaveChanges();
        }

        public void DeleteRateMembershipType(int rateType, int membershipTypeId, int year)
        {
            var d =
                _context.RateToMembershipTypes.SingleOrDefault(
                    x =>
                        x.MembershipType.Year == year && x.MembershipTypeId == membershipTypeId &&
                        x.RateTypeId == rateType);
            if (d != null)
            {
                _context.RateToMembershipTypes.Remove(d);
                _context.SaveChanges();
            }
        }

        public void DeleteFamilyMemberMembershipType(int familyMemberId, int membershipTypeId, int year)
        {
            var d =
                _context.MembershipTypeToFamilyMemberOverrides.SingleOrDefault(
                    x =>
                        x.MembershipType.Year == year && x.MembershipTypeId == membershipTypeId &&
                        x.FamilyMemberId == familyMemberId);
            if (d != null)
            {
                _context.MembershipTypeToFamilyMemberOverrides.Remove(d);
            }
            _context.SaveChanges();

        }

        public void DeleteFamilyMembershipType(int familyId, int membershipTypeId, int year)
        {
            var d =
                _context.MembershipTypeToFamilies.SingleOrDefault(
                    x =>
                        x.MembershipType.Year == year && x.MembershipTypeId == membershipTypeId &&
                        x.FamilyId == familyId);
            if (d != null)
            {
                _context.MembershipTypeToFamilies.Remove(d);
            }
            _context.SaveChanges();
        }
    }
}
