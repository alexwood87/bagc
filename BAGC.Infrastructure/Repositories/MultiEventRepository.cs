﻿using BAGC.domain.Entities;
using BAGC.domain.Repositories;

namespace BAGC.Infrastructure.Repositories
{
    public class MultiEventRepository : GenericRepository<Multi_Event>, IMultiEventRepository
    {
        public MultiEventRepository(BAGCEntities context) : base(context)
        {
        }
    }
}
