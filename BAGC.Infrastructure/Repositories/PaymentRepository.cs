﻿using System.Linq;
using BAGC.domain.Entities;
using BAGC.domain.Repositories;

namespace BAGC.Infrastructure.Repositories
{
    public class PaymentRepository : GenericRepository<Payment>, IPaymentRepository
    {
        public PaymentRepository(BAGCEntities context) : base(context)
        {
            
        }

        public void Delete(int paymentId)
        {
            var payment =_context.Payments.FirstOrDefault(x => x.PaymentID == paymentId);
            if (payment == null)
            {
                return;
            }
            _context.Payments.Remove(payment);
            
        }
    }
}
