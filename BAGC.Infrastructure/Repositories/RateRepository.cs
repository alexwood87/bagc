﻿using BAGC.domain.Entities;
using BAGC.domain.Repositories;

namespace BAGC.Infrastructure.Repositories
{
    public class RateRepository : GenericRepository<Rate>, IRateRepository
    {
        public RateRepository(BAGCEntities context) : base(context)
        {
        }
    }
}
