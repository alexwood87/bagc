﻿using System.Linq;
using BAGC.domain.Entities;
using BAGC.domain.Repositories;

namespace BAGC.Infrastructure.Repositories
{
    public class RegistrationRepository : GenericRepository<Registration>, IRegistrationRepository
    {
        public RegistrationRepository(BAGCEntities context) : base(context)
        {
        }

        public void Delete(int registrationId)
        {
            _context.Registrations.Remove(_context.Registrations.Single(x => x.RegistrationID == registrationId));
           
        }
    }
}
