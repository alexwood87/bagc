﻿using BAGC.domain.Entities;
using BAGC.domain.Repositories;
using BAGC.domain.UnitOfWorks;
using BAGC.Infrastructure.Repositories;

namespace BAGC.Infrastructure.UnitOfWorks
{
    public class UnitOfWork : IUnitOfWork
    {
        private BAGCEntities _context;

        public UnitOfWork(string connString)
        {
            _context = new BAGCEntities(connString);
        }
        private static volatile IRateRepository _rateRepository;
        private static volatile IRegistrationRepository _registrationRepository;
        private static volatile ICartRepository _cartRepository;
        private static volatile IEventRepository _eventRepository;
        private static volatile IMultiEventRepository _multiEventRepository;
        private static volatile IFamilyMemberRepository _familyMemberRepository;
        private static volatile IFamilyRepository _familyRepository;
        private static volatile IPaymentRepository _paymentRepository;
        private static volatile IMembershipTypeRepository _membershipTypeRepository;

        public IMembershipTypeRepository MembershipTypeRepository
        {
            get
            {
                return _membershipTypeRepository ?? (_membershipTypeRepository = new MembershipTypeRepository(_context));
            }
            set
            {
                _membershipTypeRepository = value;
            }
        }
        public IPaymentRepository PaymentRepository
        {
            get
            {
                return _paymentRepository ?? (_paymentRepository = new PaymentRepository(_context));
            }
            set
            {
                _paymentRepository = value;
            }
        }
        public IEventRepository EventRepository {
            get{ return _eventRepository ?? (_eventRepository = new EventRepository(_context));}
            set
            {
                _eventRepository = value;
            } 
        }

        public IFamilyMemberRepository FamilyMemberRepository
        {
            get
            {
                return _familyMemberRepository ?? (_familyMemberRepository = new FamilyMemberRepository(_context));
            }
            set
            {
                _familyMemberRepository = value;
            }
        }

        public IFamilyRepository FamilyRepository { 
            get
            {
                return _familyRepository ?? (_familyRepository = new FamilyRepository(_context));
            
            }
            set
            {
                _familyRepository = value;
            } }

        public IMultiEventRepository MultiEventRepository
        {
            get
            {
                return _multiEventRepository ?? (_multiEventRepository = new MultiEventRepository(_context));
            }
            set
            {
                _multiEventRepository = value;
            }
        }

        public ICartRepository CartRepository
        {
            get
            {
                return _cartRepository ?? (_cartRepository = new CartRepository(_context));
            }
            set
            {
                _cartRepository = value;
            }
        }

        public IRateRepository RateRepository
        {
            get
            {
                return _rateRepository ?? (_rateRepository = new RateRepository(_context));
            }
            set
            {
                _rateRepository = value;
            }
        }

        public IRegistrationRepository RegistrationRepository
        {
            get
            {
                return _registrationRepository ?? (_registrationRepository = new RegistrationRepository(_context));
            }
            set
            {
                _registrationRepository = value;

            }
        }
        public void Save()
        {
            _context.SaveChanges();
        }
    }
}
