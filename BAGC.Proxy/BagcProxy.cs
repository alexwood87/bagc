﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.ServiceModel;
using System.ServiceModel.Channels;
using BAGC.Proxy.BagcServices;

namespace BAGC.Proxy
{
    public class BagcProxy : IBagcProxy
    {
        private BagcServiceClient _client;
        public BagcProxy(string devMode)
        {
            string url;
            Binding binding;
            switch (devMode.ToLower())
            {
                case "production":
                    url = "https://wwww.bagc.com/BAGCServices/BagcService.svc?wsdl";
                    binding = new BasicHttpBinding
                    {
                        UseDefaultWebProxy = true,
                        SendTimeout = new TimeSpan(1, 0, 0),
                        CloseTimeout = new TimeSpan(1, 0, 0),
                        ReceiveTimeout = new TimeSpan(1, 0, 0)
                    };
                    break;
                case "staging":
                    binding = new BasicHttpBinding
                    {
                        UseDefaultWebProxy = true,
                        SendTimeout = new TimeSpan(1, 0, 0),
                        CloseTimeout = new TimeSpan(1, 0, 0),
                        ReceiveTimeout = new TimeSpan(1, 0, 0)
                    };
                    url = "http://localhost:55866/BagcService.svc?wsdl";
                    break;
                default:
                    binding = new BasicHttpBinding
                    {
                         UseDefaultWebProxy = true,                       
                         SendTimeout = new TimeSpan(1,0,0),
                         CloseTimeout = new TimeSpan(1,0,0),
                         ReceiveTimeout = new TimeSpan(1,0,0)
                    };
                    url = "http://localhost:55866/BagcService.svc?wsdl";
                    break;
            }
            _client = new BagcServiceClient(binding, new EndpointAddress(url));
            
        }

        public FamilyMemberModel FindHeadOfHouseByFamilyId(int familyId)
        {
            return
                _client.FindHeadOfHouseByFamilyId(familyId);
        }

        public void EditHeadOfHouse(FamilyMemberModel model)
        {
            _client.EditHeadOfHouse(model);
        }

        public void AddOrUpdateMembershipType(MembershipTypeModel model)
        {
            _client.AddOrUpdateMembershipType(model);
        }

        public List<MembershipTypeModel> LoadMembershipTypes()
        {
            var l = _client.LoadAllMembershipTypes();
            return l == null ? null : l.ToList();
        }

        public List<RegistrationModel> SearchRegistration(SerializedLinqQuery query)
        {
            var res =
                _client.SearchRegistration(query);
            return
                res != null ? res.ToList() : null;
        }

        public List<FamilyMemberRegistration> Search(SerializedLinqQuery query)
        {
            var l = _client.Search(query);
            return l == null ? null : l.ToList();
        }

        public FamilyMemberRegistration FindByFamilyMemberId(int familyMemberId)
        {
            return _client.FindFamlyMemberById(familyMemberId);
        }

        public List<FamilyMemberRegistration> FindFamilyByFamilyId(int familyId)
        {
            return _client.FindFamily(familyId).ToList();
        }

        public List<EventModel> SearchEvents(SerializedLinqQuery query)
        {
            var l =
                _client.SearchEvents(query);
            return l == null ? null : l.ToList();
        }

        public EventModel FindEventById(int eventId)
        {
            return
                _client.FindEventById(eventId);
        }

        public void AddEvent(EventModel eventModel)
        {
            _client.AddEvent(eventModel);
        }

        public void EditEvent(EventModel eventModel)
        {
            _client.EditEvent(eventModel);
        }

        public void AddFamilyMember(FamilyMemberModel model)
        {
            _client.AddFamilyMember(model);
        }

        public void EditFamilyMember(FamilyMemberModel model)
        {
            _client.EditFamilyMember(model);
        }

        public void EditRate(RateModel rateModel)
        {
            _client.EditRate(rateModel);
        }

        public void AddRate(RateModel rateModel)
        {
            _client.AddRate(rateModel);
        }

        public List<CartModel> FindCartByFamilyId(int familyId)
        {
            return _client.FindCartByFamilyId(familyId).ToList();
        }

        public CartModel FindCartByCartId(int cartId)
        {
            return _client.FindCartByCartId(cartId);
        }

        public void AddNewCart(CartModel cartModel)
        {
            _client.AddNewCart(cartModel);
        }

        public void EditCart(CartModel cartModel)
        {
            _client.EditCart(cartModel);
        }

        public int MakePayment(PaymentModel paymentModel)
        {
            return _client.MakePayment(paymentModel);
        }

        public List<PaymentModel> FindPaymentsByFamilyIdAndStartAndEndDate(int familyId, DateTime? startTime = null, DateTime? endDate = null)
        {
            var l = _client.FindPaymentsByFamilyIdAndStartAndEndDate(familyId, startTime, endDate);
                
             return l== null ? null:  l.ToList();
        }

        public void DeletePaymentAndRegistrations(int paymentId)
        {
            _client.DeletePaymentAndRegistrations(paymentId);
        }

        public void Register(RegistrationModel registrationModel)
        {
            _client.Register(registrationModel);
        }

        public RegistrationModel FindByFamilyMemberIdAndEventId(int? familyMemberId, int? eventId)
        {
            return
                _client.FindByFamilyMemberIdAndEventId(familyMemberId, eventId);
        }

        public List<EventModel> LoadAllEvents()
        {
            var l = _client.LoadAllEvents();
            return l == null ? null : l.ToList();
        }

        public List<EventModel> SearchByDescription(string desc)
        {
            var l = _client.SearchByDescription(desc);
            return l == null ? null : l.ToList();
        }

        public List<RateModel> SearchRates(SerializedLinqQuery query)
        {
            return _client.SearchRates(query).ToList();
        }

        public RateModel FindRateById(int id)
        {
            return
                _client.FindRateById(id);
        }

        public void DeleteRegistration(int registrationId)
        {
            _client.DeleteRegistration(registrationId); 
        }

        public void DeleteCart(int id)
        {
            _client.DeleteCart(id);
        }

        public FamilyMemberModel FindFamilyMemberByFamilyMemberId(int id)
        {
            return _client.FindFamilyMemberByFamilyMemberId(id);
        }
    }
}
