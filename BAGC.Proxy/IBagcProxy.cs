﻿using System;
using System.Collections.Generic;
using BAGC.Proxy.BagcServices;
namespace BAGC.Proxy
{
    public interface IBagcProxy
    {
        FamilyMemberModel FindHeadOfHouseByFamilyId(int familyId);
        void EditHeadOfHouse(FamilyMemberModel model);
        void AddOrUpdateMembershipType(MembershipTypeModel model);
        List<MembershipTypeModel> LoadMembershipTypes(); 
        List<RegistrationModel> SearchRegistration(SerializedLinqQuery query);
        List<FamilyMemberRegistration> Search(SerializedLinqQuery query);
        FamilyMemberRegistration FindByFamilyMemberId(int familyMemberId);
        List<FamilyMemberRegistration> FindFamilyByFamilyId(int familyId);
        List<EventModel> SearchEvents(SerializedLinqQuery query);
        EventModel FindEventById(int eventId);
        void AddEvent(EventModel eventModel);
        void EditEvent(EventModel eventModel);
        void AddFamilyMember(FamilyMemberModel model);
        void EditFamilyMember(FamilyMemberModel model);
        void EditRate(RateModel rateModel);
        void AddRate(RateModel rateModel);
        List<CartModel> FindCartByFamilyId(int familyId);
        CartModel FindCartByCartId(int cartId);
        void AddNewCart(CartModel cartModel);
        void EditCart(CartModel cartModel);
        int MakePayment(PaymentModel paymentModel);
        List<PaymentModel> FindPaymentsByFamilyIdAndStartAndEndDate(int familyId,
            DateTime? startTime = null, DateTime? endDate = null);
        void DeletePaymentAndRegistrations(int paymentId);
        void Register(RegistrationModel registrationModel);
        RegistrationModel FindByFamilyMemberIdAndEventId(int? familyMemberId, int? eventId);
        List<EventModel> LoadAllEvents();
        List<EventModel> SearchByDescription(string desc);
        List<RateModel> SearchRates(SerializedLinqQuery query);
        RateModel FindRateById(int id);
        void DeleteRegistration(int registrationId);
        void DeleteCart(int id);
        FamilyMemberModel FindFamilyMemberByFamilyMemberId(int id);
    }
}
