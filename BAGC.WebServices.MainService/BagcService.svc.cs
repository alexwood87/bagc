﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.Remoting;
using System.Security.Cryptography;
using BAGC.BusinessLogic;
using BAGC.Configuration;
using BAGC.Core.Contracts;
using BAGC.domain.Models;
using BAGC.domain.Models.SerializedLinq;

namespace BAGC.WebServices.MainService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class BagcService : IBagcService
    {
        private readonly IFamilyBusinessLogic _familyBusinessLogic;
        private readonly IEventBusinessLogic _eventBusinessLogic;
        private readonly IRegistrationBusinessLogic _registrationBusinessLogic;
        private readonly IPaymentBusinessLogic _paymentBusinessLogic;
        private readonly ICartBusinessLogic _cartBusinessLogic;
        public BagcService()
        {
            BagcCache.Init();
            _cartBusinessLogic = new CartBusinessLogic(ConfigurationManager.ConnectionStrings["BAGCConnString"].ConnectionString);
            _paymentBusinessLogic = new PaymentBusinessLogic(ConfigurationManager.ConnectionStrings["BAGCConnString"].ConnectionString);
            _registrationBusinessLogic = new RegistrationBusinessLogic(ConfigurationManager.ConnectionStrings["BAGCConnString"].ConnectionString);
            _eventBusinessLogic = new EventBusinessLogic(ConfigurationManager.ConnectionStrings["BAGCConnString"].ConnectionString);
            _familyBusinessLogic = new FamilyBusinessLogic(ConfigurationManager.ConnectionStrings["BAGCConnString"].ConnectionString);
        }
        public List<FamilyMemberRegistration> Search(SerializedLinqQuery searchQuery)
        {
            return
                _familyBusinessLogic.Search(searchQuery);
        }

        public FamilyMemberRegistration FindFamlyMemberById(int id)
        {
            return _familyBusinessLogic.FindById(id);
        }

        public List<FamilyMemberRegistration> SearchByExpression(CompositeExpression compositeExpression, ConstantExpression constantExpression)
        {
            return
                 null;
        }

        public List<EventModel> LoadAllEvents()
        {
            return BagcCache.GetAllEvents();
        }

        public List<EventModel> SearchByDescription(string desc)
        {
            return _eventBusinessLogic.SearchByDescription(desc);
        }

        public List<RegistrationModel> SearchRegistration(SerializedLinqQuery query)
        {
            return
                _registrationBusinessLogic.SearchRegistration(query);
        }

        public List<MembershipTypeModel> LoadAllMembershipTypes()
        {
            return
                _familyBusinessLogic.LoadAllMembershipTypes();
        }

        public FamilyMemberModel FindHeadOfHouseByFamilyId(int familyId)
        {
            return
                _familyBusinessLogic.FindHeadOfHouseByFamilyId(familyId);
        }

        public void EditHeadOfHouse(FamilyMemberModel model)
        {
             _familyBusinessLogic.EditHeadOfHouse(model);
            _familyBusinessLogic.EditFamilyMember(model);
        }

        public void AddOrUpdateMembershipType(MembershipTypeModel model)
        {
            _familyBusinessLogic.AddOrUpdateMembershipType(model);
        }

        public List<EventModel> SearchEvents(SerializedLinqQuery query)
        {
            return
                _eventBusinessLogic.SearchEvents(query);
        }

        public EventModel FindEventById(int eventId)
        {
            return _eventBusinessLogic.FindEventById(eventId);
        }

        public void AddEvent(EventModel eventModel)
        {
             _eventBusinessLogic.Add(eventModel);
        }

        public void EditEvent(EventModel eventModel)
        {
            _eventBusinessLogic.Edit(eventModel);
        }

        public void AddFamilyMember(FamilyMemberModel model)
        {
            _familyBusinessLogic.AddFamilyMember(model);
        }

        public void EditFamilyMember(FamilyMemberModel model)
        {
            _familyBusinessLogic.EditFamilyMember(model);
        }

        public void EditRate(RateModel rateModel)
        {
            _familyBusinessLogic.EditRate(rateModel);
        }

        public void AddRate(RateModel rateModel)
        {
            _familyBusinessLogic.AddRate(rateModel);
        }

        public List<CartModel> FindCartByFamilyId(int familyId)
        {
            return _cartBusinessLogic.FindCartByFamilyId(familyId);
        }

        public CartModel FindCartByCartId(int cartId)
        {
            return _cartBusinessLogic.FindCartByCartId(cartId);
        }

        public void AddNewCart(CartModel cartModel)
        {
            _cartBusinessLogic.AddNewCart(cartModel);
        }

        public void EditCart(CartModel cartModel)
        {
            _cartBusinessLogic.EditCart(cartModel);
        }

        public List<RateModel> SearchRates(SerializedLinqQuery query)
        {
            return
                _familyBusinessLogic.SearchRates(query);
        }

        public FamilyMemberModel FindFamilyMemberByFamilyMemberId(int id)
        {
            return
                _familyBusinessLogic.FindFamilyMemberByFamilyMemberId(id);
        }

        public void DeleteCart(int id)
        {
            _cartBusinessLogic.DeleteCart(id);
        }

        public void DeleteRegistration(int registrationId)
        {
            _registrationBusinessLogic.DeleteRegstration(registrationId);
        }
        public RateModel FindRateById(int id)
        {
            return _familyBusinessLogic.FindRateById(id);
        }

        public int MakePayment(PaymentModel paymentModel)
        {
            return _paymentBusinessLogic.MakePayment(paymentModel);
        }

        public List<PaymentModel> FindPaymentsByFamilyIdAndStartAndEndDate(int familyId, DateTime? startTime = null, DateTime? endDate = null)
        {
            return _paymentBusinessLogic.FindPaymentsByFamilyIdAndStartAndEndDate(familyId, startTime, endDate);
        }

        public void DeletePaymentAndRegistrations(int paymentId)
        {
            _paymentBusinessLogic.DeletePaymentAndRegistrations(paymentId);
        }

        public void Register(RegistrationModel registrationModel)
        {
            _registrationBusinessLogic.Register(registrationModel);
        }

        public RegistrationModel FindByFamilyMemberIdAndEventId(int? familyMemberId, int? eventId)
        {
            return _registrationBusinessLogic.FindByFamilyMemberIdAndEventId(familyMemberId, eventId);
        }

        public List<FamilyMemberRegistration> FindFamily(int familyId)
        {
            return
                _familyBusinessLogic.FindFamily(familyId);
        }

        public FamilyMemberRegistration FindFamilyMemberById(int id)
        {
            return
                _familyBusinessLogic.FindById(id);
        }
    }
}
