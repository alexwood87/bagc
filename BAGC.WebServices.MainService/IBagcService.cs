﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using BAGC.domain.Models;
using BAGC.domain.Models.SerializedLinq;

namespace BAGC.WebServices.MainService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IBagcService
    {

        [OperationContract]
        List<FamilyMemberRegistration> Search(SerializedLinqQuery searchQuery);
        [OperationContract]
        List<FamilyMemberRegistration> FindFamily(int familyId);
        [OperationContract]
        FamilyMemberRegistration FindFamlyMemberById(int id);
        [OperationContract]
        List<FamilyMemberRegistration> SearchByExpression(CompositeExpression compositeExpression, ConstantExpression constantExpression);
        [OperationContract]
        List<EventModel> SearchEvents(SerializedLinqQuery query);
        [OperationContract]
        EventModel FindEventById(int eventId);
        [OperationContract]
        void AddEvent(EventModel eventModel);
        [OperationContract]
        void EditEvent(EventModel eventModel);
        [OperationContract]
        void AddFamilyMember(FamilyMemberModel model);
        [OperationContract]
        void EditFamilyMember(FamilyMemberModel model);
        [OperationContract]
        void EditRate(RateModel rateModel);
        [OperationContract]
        void AddRate(RateModel rateModel);
        [OperationContract]
        List<CartModel> FindCartByFamilyId(int familyId);
        [OperationContract]
        CartModel FindCartByCartId(int cartId);
        [OperationContract]
        void AddNewCart(CartModel cartModel);
        [OperationContract]
        void EditCart(CartModel cartModel);
        [OperationContract]
        List<RateModel> SearchRates(SerializedLinqQuery query);
        [OperationContract]
        RateModel FindRateById(int id);
        [OperationContract]
        int MakePayment(PaymentModel paymentModel);
        [OperationContract]
        FamilyMemberModel FindFamilyMemberByFamilyMemberId(int id);
        [OperationContract]
        void DeleteCart(int id);
        [OperationContract]
        void DeleteRegistration(int registrationId);
        [OperationContract]
        List<PaymentModel> FindPaymentsByFamilyIdAndStartAndEndDate(int familyId,
            DateTime? startTime = null, DateTime? endDate = null);
        [OperationContract]
        void DeletePaymentAndRegistrations(int paymentId);
        [OperationContract]
        void Register(RegistrationModel registrationModel);
        [OperationContract]
        RegistrationModel FindByFamilyMemberIdAndEventId(int? familyMemberId, int? eventId);
        [OperationContract]
        List<EventModel> LoadAllEvents();
        [OperationContract]
        List<EventModel> SearchByDescription(string desc);
        [OperationContract]
        List<RegistrationModel> SearchRegistration(SerializedLinqQuery query);
        [OperationContract]
        List<MembershipTypeModel> LoadAllMembershipTypes();
        [OperationContract]
        FamilyMemberModel FindHeadOfHouseByFamilyId(int familyId);
        [OperationContract]
        void EditHeadOfHouse(FamilyMemberModel model);
        [OperationContract]
        void AddOrUpdateMembershipType(MembershipTypeModel model);
    }


}
