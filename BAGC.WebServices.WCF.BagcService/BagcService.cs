﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using BAGC.BusinessLogic;
using BAGC.domain.Models;
using BAGC.domain.Models.SerializedLinq;
using BAGC.Core.Contracts;
namespace BAGC.WebServices.WCF.BagcService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    public class BagcService : IBagcService
    {
        private readonly IFamilyBusinessLogic _familyBusinessLogic; 
        public BagcService()
        {
            _familyBusinessLogic = new FamilyBusinessLogic(ConfigurationManager.ConnectionStrings["BAGCConnString"].ConnectionString);
        }
        public List<FamilyMemberRegistration> Search(SerializedLinqQuery searchQuery)
        {
            return
                _familyBusinessLogic.Search(searchQuery);
        }

        public List<FamilyMemberRegistration> SearchByExpression(CompositeExpression compositeExpression, ConstantExpression constantExpression)
        {
            return
                null;
        }
        public List<FamilyMemberRegistration> FindFamily(int familyId)
        {
            return
                _familyBusinessLogic.FindFamily(familyId);
        }

        public FamilyMemberRegistration FindById(int id)
        {
            return
                _familyBusinessLogic.FindById(id);
        }
    }
}
