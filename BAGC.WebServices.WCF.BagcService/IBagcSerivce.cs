﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using BAGC.domain.Models;
using BAGC.domain.Models.SerializedLinq;

namespace BAGC.WebServices.WCF.BagcService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IBagcSerivce
    {
        [OperationContract]
        List<FamilyMemberRegistration> Search(SerializedLinqQuery searchQuery);
        [OperationContract]
        List<FamilyMemberRegistration> FindFamily(int familyId);
        [OperationContract]
        FamilyMemberRegistration FindById(int id);
    }

  
}
