﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BAGC.Proxy;
using BAGC.Proxy.BagcServices;

namespace BAGC.WebSites.Main.Controllers
{
    public class CartController : Controller
    {
        private readonly IBagcProxy _proxy;
        public CartController()
        {
            _proxy = new BagcProxy(ConfigurationManager.AppSettings["DevMode"]);
        }
        [HttpGet]
        public ActionResult Edit(int id)
        {
            return
                View(_proxy.FindCartByCartId(id));
        }

        [HttpPost]
        public ActionResult Edit(int id, CartModel model)
        {
            if (model.CartId <= 0)
            {
                model.CartId = id;
            }
            _proxy.EditCart(model);
            return Redirect("/Confirmation/ShowConfirmation?id="+id);
        }

        [HttpGet]
        public ActionResult Index(int familyId)
        {
            var fs = _proxy.FindFamilyByFamilyId(familyId);
            var rateSum = fs.Select(x => _proxy.FindRateById(x.RateTypeId)).Sum(x => x.Discount);
            ViewBag.TotalDiscount = rateSum;
            return
                View(_proxy.FindCartByFamilyId(familyId));
        }

        [HttpPost]
        public ActionResult Index(int familyId, PaymentModel model)
        {
            var carts = _proxy.FindCartByFamilyId(familyId);
            var registrations = new List<RegistrationModel>();
            var fam = _proxy.FindFamilyByFamilyId(familyId).OrderBy(x => x.FamilyMemberId).First();
            model.EventId = carts[0].EventId;
            model.FamilyId = fam.FamilyId;
            model.PaymentDate = DateTime.UtcNow;
            model.RateTypeId = fam.RateTypeId;
            model.Year = carts[0].Year;
            registrations.AddRange(carts.Select(x => new RegistrationModel
            {
                FamilyId = fam.FamilyId,
                Address = fam.Address,
                Attending = true,
                Comments = model.Comments,
                DateCreated = DateTime.UtcNow,
                DateUpdated = DateTime.UtcNow,
                EventId = x.EventId,
                FamilyMemberId = x.FamilyMemberId,
                Prepaid = true,
                RegistrationId = 0,
                Year = x.Year,
                Payment = model
            }));
            var id = _proxy.MakePayment(model);
            foreach (var reg in registrations)
            {
                reg.Payment = model;
                reg.FamilyMemberId = fam.FamilyMemberId;
                _proxy.Register(reg);
            }
            foreach (var cart in carts)
            {
                _proxy.DeleteCart((int)cart.CartId);
            }

            return
                Redirect("/Confirmation/ShowConfirmation?familyId=" + familyId); 
        }
        [HttpGet]
        public ActionResult Create(int familyId, int familyMemberId, int eventId)
        {
            var family = _proxy.FindByFamilyMemberId(familyMemberId);
            return 
                 View(new CartModel
                 {
                     EventId = eventId,
                     DateCreated = DateTime.UtcNow,
                     FamilyMemberId = familyMemberId,
                     FamilyId = familyId,
                     CartId = 0,
                     AmountDue = (decimal)0.0,
                     FirstName = family.FirstName,
                     LastName = family.LastName,
                     Address = family.Address,
                     Year = DateTime.UtcNow.Year.ToString(),
                     Birthday = family.BirthDate.GetValueOrDefault(DateTime.UtcNow),
                     RateTypeId = family.RateTypeId
                 });
        }

        [HttpPost]
        public ActionResult Create(int familyId, int familyMemberId, int eventId, CartModel model)
        {
            var family = _proxy.FindByFamilyMemberId(familyMemberId);
            var cart = new CartModel
            {
                EventId = eventId,
                DateCreated = DateTime.UtcNow,
                FamilyMemberId = familyMemberId,
                FamilyId = familyId,
                CartId = 0,
                AmountDue = model.AmountDue,
                FirstName = family.FirstName,
                LastName = family.LastName,
                Address = family.Address,
                Year = DateTime.UtcNow.Year.ToString(),
                Birthday = family.BirthDate.GetValueOrDefault(DateTime.UtcNow),
                RateTypeId = family.RateTypeId
            };
            _proxy.AddNewCart(cart);
            return
                Redirect("/Confirmation/ShowConfirmation?id=" + familyId + "&familyMemberId=" + familyMemberId + "&eventId=" + eventId);
        }
        // GET: Cart
        public JsonResult MakePayment(int id, PaymentModel paymentModel)
        {
            try
            {
                if (id > 0)
                {
                    paymentModel.PaymentId = id;
                }
                _proxy.MakePayment(paymentModel);
                return Json(new
                {
                    FriendlyMessage = "Payment Successfully Made",
                    Success = true
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    ex.Message,
                    Success = false
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult AddNewCart(CartModel model)
        {
            try
            {
                _proxy.AddNewCart(model);
                return Json(new
                {
                    FriendlyMessage = "Cart Successfully Made",
                    Success = true
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    ex.Message,
                    Success = false
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult EditCart(int id, CartModel model)
        {
            try
            {
                if (id > 0)
                {
                    model.CartId = id;
                }
                _proxy.EditCart(model);
                return Json(new
                {
                    FriendlyMessage = "Cart Successfully Editted",
                    Success = true
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    ex.Message,
                    Success = false
                }, JsonRequestBehavior.AllowGet);
            }    
        }

        public JsonResult DeletePayment(int paymentId)
        {
            try
            {
                _proxy.DeletePaymentAndRegistrations(paymentId);
                return Json(new
                {
                    FriendlyMessage = "Cart Successfully Editted",
                    Success = true
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    ex.Message,
                    Success = false
                }, JsonRequestBehavior.AllowGet);
            }    
        }
    }
}