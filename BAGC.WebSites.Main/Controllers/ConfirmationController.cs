﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BAGC.WebSites.Main.Controllers
{
    public class ConfirmationController : Controller
    {
        public ActionResult ShowConfirmation(int? familyId, int? familyMemberId, int? cartId, int? id)
        {
            ViewBag.Actions = new Dictionary<string, string>();
            if (Request.UrlReferrer != null)
            {
                var url = Request.UrlReferrer.AbsoluteUri.ToLower();
                if (url.Contains("/MembershipType"))
                {
                    ViewBag.ConfirmationMessage = id == null ? "Thank You. You Have Successfully Added A New Member Type": "Thank You. You have Successfully Updated A Member Type";
                    ViewBag.Actions.Add("Back To Membership Type List","/MembershipType/Index");
                }
                if (url.Contains("/rate"))
                {
                    ViewBag.Actions.Add("Rate Index", "/Rate/Index");
                    ViewBag.ConfirmationMessage = "Thank You. The Rate Type Has Been Added.";
                    if (url.Contains("/edit"))
                    {
                        ViewBag.Actions.Add("Back To Edit Rate", "/Rate/Edit/" + id.GetValueOrDefault(0));
                        ViewBag.ConfirmationMessage = "Thank You. The Rate Type Has Been Editted.";
                    }
                }
                if (url.Contains("/cart"))
                {
                    ViewBag.Actions.Add("Return To Family Detail Listing", "/Family/FamilyDetails?id=" + familyId.GetValueOrDefault(0));
                    ViewBag.Actions.Add("Return To Family Listing", "/Family/Index");
                    ViewBag.ConfirmationMessage = "Thank You. You Have Been Checked Out Successfully.";
                }
                if (url.Contains("/event"))
                {
            
                    if (id != null)
                    {
                        ViewBag.Actions.Add("Back To Editting The Event", "/Event/Edit/" + id.Value);
                        ViewBag.ConfirmationMessage = "Thank You. The Event Has Been Updated.";
                    }
                    else
                    {
                        ViewBag.ConfirmationMessage = "Thank You. The Event Has Been Added.";
                    }
                    ViewBag.Actions.Add("Back To Events", "/Event/Index");
                }
                if (url.Contains("/family"))
                {
                    if (url.Contains("/createfamily"))
                    {
                        ViewBag.ConfirmationMessage = "Thank You. A New Family Has Been Created.";
                    }
                    if (url.Contains("/editfamily"))
                    {
                        ViewBag.ConfirmationMessage = "Thank You. The Family Has Been Updated";
                    }
                    if (url.Contains("/createfamilymember"))
                    {
                        ViewBag.ConfirmationMessage = "Thank You. Your Family Member Has Been Created.";
                        ViewBag.Actions.Add("Back To The Family Member Listing",
                            "/Family/FamilyDetails?id=" + familyId.GetValueOrDefault(0));
                    }
                    if (url.Contains("/editfamilymember"))
                    {
                        ViewBag.ConfirmationMessage = "Thank You. Your Family Member Has Been Updated.";
                        ViewBag.Actions.Add("Re-Edit This Family Member", "/Family/EditFamilyMember?id=" + familyMemberId.GetValueOrDefault(0));
                        ViewBag.Actions.Add("Back To The Family Member Listing",
                            "/Family/FamilyDetails?familyId=" + familyId.GetValueOrDefault(0));
                    }
                    ViewBag.Actions.Add("Back To Family Listing","/Family/Index");
                }
            }
            else
            {
                ViewBag.ConfirmationMessage = "Thank You!";

            }
            ViewBag.Actions.Add("Go Home", "/Home");
            return View();
        }
    }
}