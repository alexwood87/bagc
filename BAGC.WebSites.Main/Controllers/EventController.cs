﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BAGC.Proxy;
using BAGC.Proxy.BagcServices;
namespace BAGC.WebSites.Main.Controllers
{
    public class EventController : Controller
    {
        private readonly IBagcProxy _proxy;
        public EventController()
        {
            _proxy = new BagcProxy(ConfigurationManager.AppSettings["DevMode"]);
        }
        // GET: Event
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult Search(string query)
        {
            try
            {
                //if (query == null || query.Trim() == "")
                //    return Json(new { ResultSet = _proxy.LoadAllEvents(), Success = true }, JsonRequestBehavior.AllowGet);
                return Json(new {ResultSet=_proxy.SearchByDescription(query), Success = true}, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return
                    Json(new
                    {
                        FriendlyMessage = "An Error Occurred",
                        DeveloperMessage = ex.ToString()
                    }, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: Event/Details/5
        public ActionResult Details(int id)
        {
            return View(_proxy.FindEventById(id));
        }

        // GET: Event/Create
        public ActionResult Create()
        {
            return View(new EventModel());
        }

        // POST: Event/Create
        [HttpPost]
        public ActionResult Create(EventModel eventModel)
        {
            try
            {
                // TODO: Add insert logic here
                _proxy.AddEvent(eventModel);
                return Redirect("/Confirmation/ShowConfirmation");
            }
            catch
            {
                return View();
            }
        }

        // GET: Event/Edit/5
        public ActionResult Edit(int id)
        {
            return View(_proxy.FindEventById(id));
        }

        // POST: Event/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, EventModel eventModel)
        {
            try
            {
                if (id > 0)
                {
                    eventModel.EventId = id;
                }
                // TODO: Add update logic here
                _proxy.EditEvent(eventModel);
                return Redirect("/Confirmation/ShowConfirmation?id=" + id);
            }
            catch
            {
                return View(_proxy.FindEventById(id));
            }
        }

        // GET: Event/Delete/5
        public ActionResult Delete(int id)
        {
           
            return View();
        }

        // POST: Event/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
