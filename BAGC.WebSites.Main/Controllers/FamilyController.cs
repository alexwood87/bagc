﻿using System;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using BAGC.Proxy.BagcServices;
using BAGC.Proxy;
using BAGC.WebSites.Main.Helpers;
using Microsoft.Ajax.Utilities;

namespace BAGC.WebSites.Main.Controllers
{
    public class FamilyController : Controller
    {
        private readonly IBagcProxy _proxy;
        public FamilyController()
        {
            _proxy = new BagcProxy(ConfigurationManager.AppSettings["DevMode"]);
        }

        public FamilyController(IBagcProxy proxy)
        {
            _proxy = proxy;
        }
        // GET: Family
        public ActionResult Index()
        {
            var list = _proxy.Search(new SerializedLinqQuery
            {
                SortProperty = "LastName",
                PageIndex = 0,
                PageSize = int.MaxValue-1,
                SortAscending = true
            });
            return View(list);
        }

        public ActionResult FamilyDetails(int id)
        {
            return
                View(_proxy.FindFamilyByFamilyId(id));
        }
        // GET: Family/Details/5
        public ActionResult Details(int id)
        {
            var fam = _proxy.FindByFamilyMemberId(id);
            return View(fam);
        }

        // GET: Family/Create
        public ActionResult CreateFamily()
        {
            return View(new FamilyMemberModel());
        }
        [HttpGet]
        public ViewResult Delete(int id)
        {
            var regs = _proxy.Search(SerializedLinqHelper.GetRegitrationId(id));
            return
                View(regs.Single());
            
        }

        [HttpGet]
        public ActionResult CreateFamilyMember(int familyId)
        {
            return
                View(new FamilyMemberModel
                {
                    FamilyId = familyId
                });
        }

        [HttpPost]
        public ActionResult CreateFamily(FamilyMemberModel model, AddressModel addModel)
        {
            model.Address = addModel;
            model.DateCreated = model.DateUpdate = DateTime.UtcNow;
            _proxy.AddFamilyMember(model);
            return
                Redirect("/Confirmation/ShowConfirmation?familyId=" + model.FamilyId);
        }
        [HttpPost]
        public ActionResult CreateFamilyMember(FamilyMemberModel model, AddressModel addressModel, int RateTypeId)
        {
            model.RateTypeId = RateTypeId;
            model.Rate = _proxy.FindRateById(RateTypeId);
            model.Address = addressModel;
            model.DateCreated = model.DateUpdate = DateTime.UtcNow;
            _proxy.AddFamilyMember(model);
            return Redirect("/Confirmation/ShowConfirmation?familyId=" + model.FamilyId + "&familyMemberId=" + model.FamilyMemberId);
        }

        [HttpGet]
        public ActionResult EditFamilyMember(int id)
        {
            return
                View(_proxy.FindFamilyMemberByFamilyMemberId(id));
        }

        [HttpPost]
        public ActionResult EditFamilyMember(int? id, FamilyMemberModel model)
        {
            if (id != null && id > 0)
            {
                model.FamilyMemberId = id.Value;
            }
            var fam = _proxy.FindFamilyMemberByFamilyMemberId(id.GetValueOrDefault(0));
            model.FamilyId = fam.FamilyId;
            model.DateCreated = fam.DateCreated;
            model.DateUpdate = DateTime.UtcNow;
            _proxy.EditFamilyMember(model);
            return Redirect("/Confirmation/ShowConfirmation?familyId=" + model.FamilyId + "&familyMemberId=" + id.GetValueOrDefault(0));
        }
        [HttpPost]
        public ActionResult EditFamilyRegistration(int id, FamilyMemberRegistration registration, PaymentModel payment)
        {
            if (registration.DateCreated == DateTime.MinValue)
            {
                registration.DateCreated = DateTime.UtcNow;
            }
            registration.DateUpdate = DateTime.UtcNow;
            _proxy.Register(new RegistrationModel
            {
                Address = registration.Address,
                Year = registration.Year,
                Attending = true,
                Comments = registration.Comments,
                DateCreated = registration.DateCreated,
                DateUpdated = registration.DateUpdate,
                EventId = registration.EventId,
                FamilyId = registration.FamilyId,
                FamilyMemberId = registration.FamilyMemberId,
                Payment = payment,
                Prepaid = true,
                RegistrationId = id
            });
            return Redirect("/Family/FamilyDetails?id=" + registration.FamilyId);
        }
       
        [HttpPost]
        public ActionResult CreateFamilyRegistration(FamilyMemberRegistration registration, PaymentModel payment)
        {
            registration.DateCreated = registration.DateUpdate = DateTime.UtcNow;
            _proxy.Register(new RegistrationModel
            {
                Address = registration.Address,
                Year = registration.Year,
                Attending = true,
                Comments = registration.Comments,
                DateCreated = registration.DateCreated,
                DateUpdated = registration.DateUpdate,
                EventId = registration.EventId,
                FamilyId = registration.FamilyId,
                FamilyMemberId = registration.FamilyMemberId,
                Payment = payment,
                Prepaid = true,
                RegistrationId = 0
            });
            return Redirect("/Family/FamilyDetails?id=" + registration.FamilyId);;
        }
        [HttpGet]
        public ViewResult CreateRegistration(int familyId, int familyMemberId)
        {
            
            return
                View(new RegistrationModel
                {
                    FamilyId = familyId,
                    FamilyMemberId = familyMemberId,
                    DateCreated = DateTime.UtcNow,
                    DateUpdated = DateTime.UtcNow
                });
        }

        [HttpPost]
        public ActionResult CreateRegistration(RegistrationModel model)
        {
            var add = _proxy.FindByFamilyMemberId(model.FamilyMemberId).Address;
            model.DateCreated = model.DateUpdated = DateTime.UtcNow;
            model.Address = add;
            _proxy.Register(model);
            return RedirectToAction("Index");
        }
        [HttpPost]
        public ActionResult DeleteFamilyRegistration(int? id, FamilyMemberRegistration registration)
        {
            _proxy.DeleteRegistration(id == null || id <= 0 ? registration.RegistrationId : id.Value);
            return RedirectToAction("Index");
        }
        public JsonResult Search(string lastName, string firstName)
        {
            try
            {
               
                if (!string.IsNullOrEmpty(lastName) && !string.IsNullOrEmpty(firstName))
                {


                    return
                        Json(new
                        {
                            ResultSet = _proxy.Search(new SerializedLinqQuery
                            {
                                CompositeExpression = new CompositeExpression
                                {
                                    LeftExpression = new ConstantExpression
                                    {
                                        Type = SerializedType.String,
                                        ConditionalOperator = ConditionalOperator.Contains,
                                        PropertyName = "LastName",
                                        Value = lastName
                                    },
                                    LogicOperator = LogicOperator.AND,
                                    RightExpression = new ConstantExpression
                                    {
                                        ConditionalOperator = ConditionalOperator.Contains,
                                        PropertyName = "FirstName",
                                        Type = SerializedType.String,
                                        Value = firstName
                                    }
                                }, PageIndex = 0, PageSize = int.MaxValue,SortAscending = true, SortProperty = "LastName"
                            }),
                            Success = true
                        }, JsonRequestBehavior.AllowGet);
                }
                if (!string.IsNullOrEmpty(lastName))
                {
                    return
                        Json(new {ResultSet = _proxy.Search(new SerializedLinqQuery
                        {
                            ConstantExpression = new ConstantExpression
                            {
                                ConditionalOperator = ConditionalOperator.Contains,
                                Type = SerializedType.String,
                                PropertyName = "LastName",
                                Value = lastName
                            },
                            PageIndex = 0,
                            PageSize = 100,
                            SortAscending = true,
                            SortProperty = "LastName"
                        }),
                                  Success = true
                        }, JsonRequestBehavior.AllowGet);
                }
                if (!string.IsNullOrEmpty(firstName))
                {
                    return
                        Json(new
                        {
                            ResultSet = _proxy.Search(new SerializedLinqQuery
                            {
                                ConstantExpression = new ConstantExpression
                                {
                                    ConditionalOperator = ConditionalOperator.Contains,
                                    Type = SerializedType.String,
                                    PropertyName = "FirstName",
                                    Value = firstName
                                },
                                PageIndex = 0,
                                PageSize = 100,
                                SortAscending = true,
                                SortProperty = "FirstName"
                            }),
                            Success = true
                        }, JsonRequestBehavior.AllowGet);
                }
                return
                          Json(new
                          {
                              ResultSet = _proxy.Search(new SerializedLinqQuery
                              {
                                  PageIndex = 0,
                                  PageSize = int.MaxValue,
                                  SortAscending = true,
                                  SortProperty = "LastName"
                              }),
                              Success = true
                          }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    DeveloperMessage = ex.ToString(),
                    Success = false,
                    FriendlyMessage = "AN Error Occurred" 
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public ActionResult CreatePayment(int familyId)
        {
            return
                View(new PaymentModel
                {
                    FamilyId = familyId
                });
        }

        [HttpGet]
        public ActionResult PaymentIndex(int familyId)
        {
            var res = _proxy.FindPaymentsByFamilyIdAndStartAndEndDate(familyId,
                DateTime.Parse("1/1/" + DateTime.Today.Year), DateTime.Parse("12/31/" + DateTime.Today.Year));
            return
                View(res);
        }
        [HttpGet]
        public ActionResult DeletePayment(int id)
        {
            _proxy.DeletePaymentAndRegistrations(id);
            return
                RedirectToAction("Index");
        }
        [HttpPost]
        public ActionResult CreatePayment(PaymentModel model)
        {
            model.PaymentDate = DateTime.UtcNow;
            _proxy.MakePayment(model);
            return Redirect("/Family/FamilyDetails?id=" + model.FamilyId);;
        }
        
        
        [HttpGet]
        // GET: Family/Edit/5
        public ActionResult EditFamilyRegistration(int id)
        {
            var fam = _proxy.Search(SerializedLinqHelper.GetRegitrationId(id)).First();
            return View(fam);
        }

        [HttpGet]
        public ActionResult EditRegistration(int familyMemberId, int eventId)
        {
            return
                View(_proxy.FindByFamilyMemberIdAndEventId(familyMemberId, eventId));
        }

        [HttpGet]
        public ActionResult EditRateForFamilyMember(int familyMemberId)
        {
            var f = _proxy.FindByFamilyMemberId(familyMemberId);
            var carts = _proxy.FindCartByFamilyId(f.FamilyId);
            if (carts != null && carts.Any())
            {
                var c = carts.FirstOrDefault(x => x.FamilyMemberId == familyMemberId);
                if (c != null)
                {
                    f.BirthDate = c.Birthday;
                }
            }
            return
                View(f);
        }

        [HttpPost]
        public void EditRateForFamilyMember(int familyMemberId, FamilyMemberRegistration fam)
        {
            var rate = _proxy.SearchRates(new SerializedLinqQuery
            {
                ConstantExpression = new ConstantExpression
                {
                    Value = fam.RateTypeId.ToString(),
                    PropertyName = "RateTypeID",
                    Type = SerializedType.Int32,
                    ConditionalOperator = ConditionalOperator.Equal                  
                },
            }).Single();
            var f1 = _proxy.FindByFamilyMemberId(familyMemberId);
            _proxy.EditFamilyMember(
                new FamilyMemberModel
                {
                    RateTypeId = fam.RateTypeId,
                    Rate = rate,
                    Address = f1.Address,
                    FamilyId = f1.FamilyId,
                    Comments = f1.Comments,
                    DateCreated = DateTime.UtcNow,
                    DateUpdate = DateTime.UtcNow,
                    FamilyMemberId = f1.FamilyMemberId,
                    FirstName = f1.FirstName,
                    LastName = f1.LastName
                }
            );
            if (f1.RateTypeId <= 0)
            {
                _proxy.AddNewCart(new CartModel
                {
                    RateTypeId = rate.RateType,
                    AmountDue = rate.Amount,
                    Address = f1.Address,
                    FamilyId = f1.FamilyId,
                    EventId = rate.EventId,
                    Birthday = fam.BirthDate.GetValueOrDefault(DateTime.MinValue),
                    CartId = 0,
                    DateCreated = DateTime.UtcNow,
                    FamilyMemberId = f1.FamilyMemberId,
                    FirstName = f1.FirstName,
                    LastName = f1.LastName,
                    Year = rate.Year
                });
            }
            else
            {
                var cart =
                    _proxy.FindCartByFamilyId(f1.FamilyId).FirstOrDefault(x => x.FamilyMemberId == f1.FamilyMemberId);
                if (cart != null)
                {
                    cart.AmountDue = rate.Amount;
                    cart.Address = f1.Address;
                    cart.Birthday = fam.BirthDate.GetValueOrDefault(DateTime.MinValue);
                    cart.EventId = rate.EventId;
                    cart.RateTypeId = rate.RateType;
                    cart.Year = rate.Year;
                    _proxy.EditCart(cart);
                }
            }

            //return Redirect("/Family/FamilyDetails?id=" + f1.FamilyId);
        }

        [HttpGet]
        public ActionResult EditFamily(int familyId)
        {
            var fam = _proxy.FindHeadOfHouseByFamilyId(familyId);
            return View(fam);
        }

        [HttpPost]
        public ActionResult EditFamily(int familyId, FamilyMemberModel model, string StateOrProvince)
        {
            if (!string.IsNullOrEmpty(StateOrProvince))
            {
                model.Address.StateOrProvince = StateOrProvince;
            }
            model.DateCreated = model.DateUpdate = DateTime.UtcNow;
            var fam = _proxy.FindHeadOfHouseByFamilyId(familyId);
            model.FamilyMemberId = fam.FamilyMemberId;
            model.FamilyId = familyId;
            _proxy.EditHeadOfHouse(model);
            return Redirect("/Confirmation/ShowConfirmation?familyId="+familyId);
        }
        [HttpPost]
        public ActionResult EditRegistration(int? id, int? registrationId, RegistrationModel model)
        {
            if (id != null && id > 0)
            {
                registrationId = id;
            }
            if (registrationId != null)
            {
                model.RegistrationId = registrationId.Value;
            }
            var add = _proxy.FindByFamilyMemberId(model.FamilyMemberId).Address;
            model.Address = add;
            _proxy.Register(model);
            return Redirect("/Family/FamilyDetails?id=" + model.FamilyId);;
        }
        // GET: Family/Delete/5
        [HttpGet]
        public ActionResult DeleteFamilyRegistration(int id)
        {
            return View(_proxy.Search(SerializedLinqHelper.GetRegitrationId(id)).First());
        }

        [HttpPost]
        public ActionResult DeleteRegistration(int? id, RegistrationModel model)
        {
            _proxy.DeleteRegistration(id != null ? id.Value : model.RegistrationId);
            return Redirect("/Family/FamilyDetails?id=" + model.FamilyId);
        }
        
    }
}
