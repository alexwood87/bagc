﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BAGC.Proxy;
using BAGC.Proxy.BagcServices;
using BAGC.WebSites.Main.Helpers;

namespace BAGC.WebSites.Main.Controllers
{
    public class MembershipTypeController : Controller
    {
        private readonly IBagcProxy _proxy;
        public MembershipTypeController()
        {
            _proxy = new BagcProxy(ConfigurationManager.AppSettings["DevMode"]);
        }
        // GET: MembershipType
        [HttpGet]
        public ActionResult Index()
        {
            return View(SessionHelper.GetAllMembershipTypes());
        }

        [HttpGet]
        public ActionResult Edit(int membershipTypeId)
        {
            return
                View(SessionHelper.GetAllMembershipTypes().SingleOrDefault(x => x.MembershipTypeId == membershipTypeId));
        }

        [HttpPost]
        public ActionResult Edit(int membershipTypeId, MembershipTypeModel model)
        {
            if (membershipTypeId > 0)
            {
                model.MembershipTypeId = membershipTypeId;
            }
            _proxy.AddOrUpdateMembershipType(model);
            SessionHelper.Init();
            return
                Redirect("/Confirmation/ShowConfirmation?id=" + membershipTypeId);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return
                View(new MembershipTypeModel());
        }

        [HttpPost]
        public ActionResult Create(MembershipTypeModel model)
        {
            _proxy.AddOrUpdateMembershipType(model);
            SessionHelper.Init();
            return Redirect("/Confirmation/ShowConfirmation");
        }
    }
}