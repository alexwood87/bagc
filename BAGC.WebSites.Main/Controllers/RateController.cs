﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BAGC.Proxy;
using BAGC.Proxy.BagcServices;

namespace BAGC.WebSites.Main.Controllers
{
    public class RateController : Controller
    {
        private readonly IBagcProxy _proxy;
        public RateController()
        {
            _proxy = new BagcProxy(ConfigurationManager.AppSettings["DevMode"]);
        }

        [HttpGet]
        public ViewResult Index()
        {
            return View(_proxy.SearchRates(new SerializedLinqQuery
            {
                PageIndex = 0,
                PageSize = int.MaxValue,
                SortAscending = true,
                SortProperty = null
            }));
        }
        [HttpGet]
        public ViewResult Edit(int id)
        {
            return
                View(_proxy.FindRateById(id));
        }
        [HttpPost]
        public ActionResult Edit(int id, RateModel model)
        {
            if (model.RateType <= 0)
            {
                model.RateType = id;
            }
            if (model.DateCreated <= DateTime.MinValue)
            {
                model.DateCreated = DateTime.UtcNow;
            }
            model.DateUpdated = DateTime.UtcNow;
            _proxy.EditRate(model);
            return Redirect("/Confirmation/ShowConfirmation?id=" + id);
        }

        [HttpGet]
        public ViewResult Create()
        {
            return
                View(new RateModel
                {
                    DateCreated = DateTime.UtcNow,
                    DateUpdated = DateTime.UtcNow
                });
        }

        [HttpPost]
        public ActionResult Create(RateModel model)
        {
            model.DateCreated = model.DateUpdated = DateTime.UtcNow;
            _proxy.AddRate(model);
            return Redirect("/Confirmation/ShowConfirmation");
        }
    }
}