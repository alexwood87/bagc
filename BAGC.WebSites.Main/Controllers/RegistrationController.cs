﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BAGC.Proxy;
using BAGC.Proxy.BagcServices;
using BAGC.WebSites.Main.Helpers;

namespace BAGC.WebSites.Main.Controllers
{
    public class RegistrationController : Controller
    {
        private readonly IBagcProxy _proxy;
        public RegistrationController()
        {
            _proxy = new BagcProxy(ConfigurationManager.AppSettings["DevMode"]);
        }
        // GET: Registration
        public ActionResult Index(int familyId)
        {
            return View(_proxy.SearchRegistration(new SerializedLinqQuery
            {
                ConstantExpression = new ConstantExpression
                {
                    PropertyName = "FamilyID",
                    Type = SerializedType.Int32,
                    Value = familyId.ToString(),
                    ConditionalOperator = ConditionalOperator.Equal
                },
                PageIndex = 0,
                PageSize = int.MaxValue,
                SortAscending = true,
                SortProperty = "FamilyMemberID"
                
            }));
        }
        [HttpGet]
        public ActionResult Details(int id)
        {
            return
                View(_proxy.SearchRegistration(SerializedLinqHelper.GetRegitrationId(id)).First());
        }
    }
}