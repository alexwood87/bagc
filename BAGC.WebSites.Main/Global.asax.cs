﻿using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using BAGC.WebSites.Main;
using BAGC.WebSites.Main.Helpers;

namespace BAGC.WebSites.Main
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            
        }

        protected void Session_Start()
        {
            SessionHelper.Init();  
        }
    }
}
