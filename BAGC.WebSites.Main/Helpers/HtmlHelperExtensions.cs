﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace BAGC.WebSites.Main.Helpers
{
    public static class HtmlHelperExtensions
    {
        public static MvcHtmlString DisplayForEvents(this HtmlHelper html, int selectedItemId)
        {
            return html.DropDownList("EventId", SessionHelper.GetAllEvents().Select(x => new SelectListItem
            {
                Disabled = false,
                Selected = x.EventId == selectedItemId,
                Text = x.Description,
                Value = x.EventId.ToString()
            }));
        }

        public static MvcHtmlString DisplayForMemberTypes(this HtmlHelper html, int membershipTypeId)
        {
            var l = SessionHelper.GetAllMembershipTypes();
            if (l != null)
                return
                    html.DropDownList("MembershipTypeId", l.Select(x => new SelectListItem
                    {
                        Disabled = false,
                        Value = x.MembershipTypeId.ToString(CultureInfo.InvariantCulture),
                        Selected = x.MembershipTypeId == membershipTypeId,
                        Text = x.Name
                    }));
            return
                html.DropDownList("MembershipTypeId");
        }

        public static MvcHtmlString DisplayForStates(this HtmlHelper html, string stateSelected, string statesDropDownName = "Address.StateOrProvince")
        {
            if (stateSelected != null)
            {
                stateSelected = stateSelected.Trim();
            }
            /*
           Alabama 	AL 	Montana 	MT
Alaska 	AK 	Nebraska 	NE
Arizona 	AZ 	Nevada 	NV
Arkansas 	AR 	New Hampshire 	NH
California 	CA 	New Jersey 	NJ
Colorado 	CO 	New Mexico 	NM
Connecticut 	CT 	New York 	NY
Delaware 	DE 	North Carolina 	NC
Florida 	FL 	North Dakota 	ND
Georgia 	GA 	Ohio 	OH
Hawaii 	HI 	Oklahoma 	OK
Idaho 	ID 	Oregon 	OR
Illinois 	IL 	Pennsylvania 	PA
Indiana 	IN 	Rhode Island 	RI
Iowa 	IA 	South Carolina 	SC
Kansas 	KS 	South Dakota 	SD
Kentucky 	KY 	Tennessee 	TN
Louisiana 	LA 	Texas 	TX
Maine 	ME 	Utah 	UT
Maryland 	MD 	Vermont 	VT
Massachusetts 	MA 	Virginia 	VA
Michigan 	MI 	Washington 	WA
Minnesota 	MN 	West Virginia 	WV
Mississippi 	MS 	Wisconsin 	WI
Missouri 	MO 	Wyoming 	WY
           */
            var list = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("AL", "Alabama"),
                new KeyValuePair<string, string>("AK", "Alaska"),
                new KeyValuePair<string, string>("AR", "Arkansas"),
                new KeyValuePair<string, string>("CA", "California"),
                new KeyValuePair<string, string>("CO", "Colorado"),
                new KeyValuePair<string, string>("CT", "Connecticut"),
                new KeyValuePair<string, string>("DE", "Delaware"),
                new KeyValuePair<string, string>("FL", "Florida"),
                new KeyValuePair<string, string>("GA", "Georgia"),
                new KeyValuePair<string, string>("HI", "Hawaii"),
                new KeyValuePair<string, string>("IA", "Iowa"),
                new KeyValuePair<string, string>("ID", "Idaho"),              
                new KeyValuePair<string, string>("IL","Illinois"),
                new KeyValuePair<string, string>("IN", "Indiana"),
                new KeyValuePair<string, string>("KS", "Kansas"),
                new KeyValuePair<string, string>("KY", "Kentucky"),
                new KeyValuePair<string, string>("LA", "Louisiana"),
                new KeyValuePair<string, string>("ME", "Maine"),
                new KeyValuePair<string, string>("MD", "Maryland"),
                new KeyValuePair<string, string>("MA", "Massachusetts"),
                new KeyValuePair<string, string>("MI", "Michigan"),
                new KeyValuePair<string, string>("MN", "Minnesota"),
                new KeyValuePair<string, string>("MS", "Mississippi"),
                new KeyValuePair<string, string>("MO", "Missouri"),
                new KeyValuePair<string, string>("MT", "Montana"),
                new KeyValuePair<string, string>("NE", "Nebraska"),
                new KeyValuePair<string, string>("NV", "Nevada"),
                new KeyValuePair<string, string>("NH", "New Hampshire"),
                new KeyValuePair<string, string>("NJ", "New Jersey"),
                new KeyValuePair<string, string>("NM", "New Mexico"),
                new KeyValuePair<string, string>("NY", "New York"),
                new KeyValuePair<string, string>("NC", "North Carolina"),
                new KeyValuePair<string, string>("ND", "North Dakota"),
                new KeyValuePair<string, string>("OH", "Ohio"),
                new KeyValuePair<string, string>("OK", "Oklahoma"),
                new KeyValuePair<string, string>("OR", "Oregon"),
                new KeyValuePair<string, string>("PA", "Pennsylvania"),
                new KeyValuePair<string, string>("RI", "Rhode Island"),
                new KeyValuePair<string, string>("SC", "South Carolina"),
                new KeyValuePair<string, string>("SD", "South Dakota"),
                new KeyValuePair<string, string>("TN", "Tennessee"),
                new KeyValuePair<string, string>("TX", "Texas"),
                new KeyValuePair<string, string>("UT", "Utah"),
                new KeyValuePair<string, string>("VT", "Vermont"),
                new KeyValuePair<string, string>("VA", "Virginia"),
                new KeyValuePair<string, string>("WA", "Washington"),
                new KeyValuePair<string, string>("WV", "West Virginia"),
                new KeyValuePair<string, string>("WI", "Wisconsin"),
                new KeyValuePair<string, string>("WY", "Wyoming")
            };
            var li = list.Select(x => new SelectListItem
                {
                    Disabled = false,
                    Text = x.Value,
                    Value = x.Key,
                    Selected = x.Key == stateSelected
                });
          
            return
                html.DropDownList(statesDropDownName, li,"State",new{value=stateSelected});
        }
        public static MvcHtmlString DisplayForYears(this HtmlHelper html, string selectedYear)
        {
            var years = new List<string>();
            for (var i = DateTime.UtcNow.AddYears(-10).Year; i < DateTime.UtcNow.AddYears(2).Year; i++)
            {
                years.Add(i.ToString(CultureInfo.InvariantCulture));
            }
            return
                html.DropDownList("Year", years.Select(x => new SelectListItem
                {
                    Selected = x == selectedYear,
                    Value = x,
                    Text = x,
                    Disabled = false
                }));
        }

        public static MvcHtmlString DisplayForRatings(this HtmlHelper html, int selectedItemId, int membershipTypeId)
        {
            var proxy = new BAGC.Proxy.BagcProxy(ConfigurationManager.AppSettings["DevMode"]);
            var rates = proxy.SearchRates(null);
            return
                html.DropDownList("RateTypeId", rates.Where(x => x.MembershipType != null && x.MembershipType.MembershipTypeId == membershipTypeId).Select(x => new SelectListItem
                {
                    Disabled = false,
                    Selected = x.RateType == selectedItemId,
                    Text = string.Format("Amount: {0} Child: {1} Discounted: {2} EventId: {3} Comments {4}",x.Amount, x.IsChild, x.Discount, x.EventId, x.Comments??""),
                    Value = x.RateType.ToString(CultureInfo.InvariantCulture)
                }));
        }
    }
}