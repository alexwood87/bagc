﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BAGC.Proxy.BagcServices;

namespace BAGC.WebSites.Main.Helpers
{
    public class SerializedLinqHelper
    {
        public static SerializedLinqQuery GetRegitrationId(int id)
        {
            return
                new SerializedLinqQuery
                {
                    PageSize = 1,
                    PageIndex = 0,
                    ConstantExpression = new ConstantExpression
                    {
                        ConditionalOperator = ConditionalOperator.Equal,
                        PropertyName = "RegitrationID",
                        Value = id.ToString(),
                        Type = SerializedType.Int32
                    }
                };
        }
    }
}