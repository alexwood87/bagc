﻿var eventApp = angular.module("EventsApp", []);
eventApp.controller("EventListController", ["$scope", "$http", function ($scope, $http) {
   
    $scope.Events = [];
    $scope.Query = "";
    $scope.goList = function () {
        $http.get("/Event/Search?query=" + $scope.Query).then(function (response) {
            return response.data;
        }).then(function (data) {
            if (data.Success) {
                $scope.Events = data.ResultSet;
              
            } else {
                alert(data.FriendlyMessage);
            }
        });
    };
    $scope.goFamilyDetails = function (path) {
        window.location.href = path;
    };
}]);