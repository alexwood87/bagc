﻿var familySearchApp = angular.module("FamilySearchApp", []);
familySearchApp.controller("FamilySearchController", ["$scope", "$http", function($scope, $http) {
    $scope.FirstName = "";
    $scope.LastName = "";
    $scope.people = [];
    $scope.goList = function() {
        $http.get("/Family/Search?firstName=" + $scope.FirstName + "&lastName=" + $scope.LastName).then(function(response) {
            return response.data;
        }).then(function(data) {
            if (data.Success) {
                $scope.people = data.ResultSet;
            } else {
                alert(data.FriendlyMessage);
            }
        });
    };
    $scope.goFamilyDetails = function(path) {
        window.location.href = path;
    };
}]);