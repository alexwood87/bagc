﻿using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using BAGC.Proxy;
using BAGC.Proxy.BagcServices;

namespace BAGC.WebSites.Main.Helpers
{
    public static class SessionHelper
    {
        public static void Init()
        {
            var api = new BagcProxy(ConfigurationManager.AppSettings["DevMode"]);
            HttpContext.Current.Session["AllEvents"] = api.SearchEvents(new SerializedLinqQuery
            {
                PageSize = int.MaxValue,
                PageIndex = 0,
                SortProperty = "EventDesc",
                SortAscending = true               
            });
            HttpContext.Current.Session["AllMembershipTypes"] = api.LoadMembershipTypes();
        }

        public static List<EventModel> GetAllEvents()
        {
            return
                (List<EventModel>) HttpContext.Current.Session["AllEvents"];
        }
        public static ListItemCollection LoadEventsForDropDown()
        {
            var list = new ListItemCollection();
            if (HttpContext.Current.Session["AllEvents"] == null)
            {
                return list;
            }
            list.AddRange(GetAllEvents().Select(x => new ListItem(x.Description, x.EventId.ToString(CultureInfo.InvariantCulture))).ToArray());
            return list;
        }

        public static List<MembershipTypeModel> GetAllMembershipTypes()
        {
            return
                (List<MembershipTypeModel>) HttpContext.Current.Session["AllMembershipTypes"];
        }
    }
}