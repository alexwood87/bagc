﻿using System;

namespace BAGC.domain.Exceptions
{
    public class AddressInValidException : Exception
    {
        public AddressInValidException(string message) : base(message)
        {
            
        }
    }
}
