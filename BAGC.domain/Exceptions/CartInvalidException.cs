﻿using System;

namespace BAGC.domain.Exceptions
{
    public class CartInvalidException : Exception
    {
        public CartInvalidException(string msg) : base(msg)
        {
            
        }
    }
}
