﻿using System;

namespace BAGC.domain.Exceptions
{
    public class EventInvalidException : Exception
    {
        public EventInvalidException(string message) : base(message)
        {
            
        }
    }
}
