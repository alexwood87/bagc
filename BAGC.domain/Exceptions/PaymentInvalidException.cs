﻿using System;

namespace BAGC.domain.Exceptions
{
    public class PaymentInvalidException : Exception
    {
        public PaymentInvalidException(string message) : base(message)
        {
            
        }
    }
}
