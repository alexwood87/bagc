﻿using System;

namespace BAGC.domain.Exceptions
{
    public class RegistrationInvalidException : Exception
    {
        public RegistrationInvalidException(string message) : base(message)
        {
            
        }
    }
}
