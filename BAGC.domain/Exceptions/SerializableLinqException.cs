﻿using System;

namespace BAGC.domain.Exceptions
{
    public class SerializableLinqException : Exception
    {
        public SerializableLinqException(string msg) : base(msg)
        {
            
        }
    }
}
