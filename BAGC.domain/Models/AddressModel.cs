﻿using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using BAGC.domain.Exceptions;

namespace BAGC.domain.Models
{
    [DataContract]
    public class AddressModel : IValidate
    {
        [DataMember]
        public string AddressLine1 { get; set; }
        [DataMember]
        public string AddressLine2 { get; set; }
        [DataMember]
        public string AddressLine3 { get; set; }
        [DataMember]
        public string City { get; set; }
        [DataMember]
        public string StateOrProvince { get; set; }
        [DataMember]
        public string ZipCode { get; set; }
        [DataMember]
        public string PhoneNumber { get; set; }
        [DataMember]
        public string Email { get; set; }
        private static readonly Regex EmailRegex = new Regex(@"^[A-Za-z0-9._%+-]{1,64}@(?:[A-Za-z0-9-]{1,63}\.){1,125}[A-Za-z]{2,63}$");
        public void Validate()
        {
            if (string.IsNullOrEmpty(Email))
            {
                throw new AddressInValidException("Email Is Required");
            }
            if (!EmailRegex.IsMatch(Email.Trim()))
            {
                throw new AddressInValidException("Email is not in the required format");
            }
            if (string.IsNullOrEmpty(AddressLine1))
            {
                throw new AddressInValidException("Address Line 1 Is Required");
            }
            if (string.IsNullOrEmpty(City))
            {
                throw new AddressInValidException("City Is Required");
            }
            if (string.IsNullOrEmpty(PhoneNumber))
            {
                throw new AddressInValidException("Phone Number Is Required");
            }

        }
    }
}
