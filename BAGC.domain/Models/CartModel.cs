﻿using System;
using System.Runtime.Serialization;
using BAGC.domain.Exceptions;

namespace BAGC.domain.Models
{
    [DataContract]
    public class CartModel : IValidate
    {
        [DataMember]
        public long CartId { get; set; }
        [DataMember]
        public string Year { get; set; }

        [DataMember]
        public int EventId { get; set; }

        [DataMember]
        public int FamilyId { get; set; }

        [DataMember]
        public int FamilyMemberId { get; set; }

        [DataMember]
        public string LastName { get; set; }

        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public DateTime Birthday { get; set; }

        [DataMember]
        public AddressModel Address { get; set; }

        [DataMember]
        public int RateTypeId { get; set; }

        [DataMember]
        public decimal AmountDue { get; set; }

        [DataMember]
        public DateTime DateCreated { get; set; }

        public void Validate()
        {
            if (string.IsNullOrEmpty(LastName) || string.IsNullOrEmpty(FirstName))
            {
                throw new CartInvalidException("Name is required");
            }
            if (EventId <= 0)
            {
                throw new CartInvalidException("Event Id is required");
            }
            Address.Validate();
            if (AmountDue < 0)
            {
                throw new CartInvalidException("Amount Due Cannot Be Negative");
            }
            if (Birthday != null && (Birthday > DateTime.UtcNow || Birthday <= DateTime.MinValue)) 
            {
                throw new CartInvalidException("invalid Birthday");
            }
        }
    }
}

