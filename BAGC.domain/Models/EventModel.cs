﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using BAGC.domain.Exceptions;

namespace BAGC.domain.Models
{
    [DataContract]
    public class EventModel : IValidate
    {
        [DataMember]
        public int EventId { get; set; }
        [DataMember]
        public string EventName { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public DateTime EventDate { get; set; }
        [DataMember]
        public bool IsMultiPartEvent { get; set; }
        [DataMember]
        public bool Discount { get; set; }

        public void Validate()
        {
            if (string.IsNullOrEmpty(Description))
            {
                throw new EventInvalidException("Description is required");
            }
            if (EventDate == DateTime.MinValue)
            {
                throw new EventInvalidException("Event Date is required");
            }
           
        }
    }
}
