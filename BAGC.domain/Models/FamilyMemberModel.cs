﻿using System;
using System.Runtime.Serialization;
using BAGC.domain.Exceptions;

namespace BAGC.domain.Models
{
    [DataContract]
    public class FamilyMemberModel : IValidate
    {
        [DataMember]
        public int FamilyId { get; set; }
        [DataMember]
        public int FamilyMemberId { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public string FirstName { get; set; }
        [DataMember]
        public AddressModel Address { get; set; }
        [DataMember]
        public int RateTypeId { get; set; }
        [DataMember]
        public RateModel Rate { get; set; }
        [DataMember]
        public DateTime DateCreated { get; set; }
        [DataMember]
        public DateTime DateUpdate { get; set; }
        [DataMember]
        public string Comments { get; set; }
        [DataMember]
        public MembershipTypeModel MembershipType { get; set; }
        [DataMember]
        public int? MembershipTypeId { get; set; }
        
        public void Validate()
        {
            
            Address.Validate();
            if (MembershipType != null)
            {
                MembershipType.Validate();
            }
            if (Rate != null)
            {
                Rate.Validate();
            }
            if (FamilyMemberId < 0)
            {
                throw new RegistrationInvalidException("RegistrationId is required or must be 0");
            }
            if ((Rate != null && RateTypeId < 1) || FamilyId < 0 || FamilyMemberId < 0)
            {
                throw new RegistrationInvalidException("Year, RateTypeId, EventId, FamilyId, FamilyMemberId are required");
            }
            if (string.IsNullOrEmpty(FirstName) || string.IsNullOrEmpty(LastName))
            {
                throw new RegistrationInvalidException("Name is invalid");
            }
            
        }
        
    }
}
