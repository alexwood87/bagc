﻿using System;
using System.Runtime.Serialization;
using BAGC.domain.Exceptions;

namespace BAGC.domain.Models
{
    [DataContract]
    public class FamilyMemberRegistration : IValidate
    {
        [DataMember]
        public int RegistrationId { get; set; }
        [DataMember]
        public string Year { get; set; }

        [DataMember]
        public int EventId { get; set; }

        [DataMember]
        public int FamilyId { get; set; }

        [DataMember]
        public int FamilyMemberId { get; set; }

        [DataMember]
        public string LastName { get; set; }

        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public DateTime? BirthDate { get; set; }
        [DataMember]
        public DateTime DateCreated { get; set; }
        [DataMember]
        public DateTime DateUpdate { get; set; }
        [DataMember]
        public string Comments { get; set; }
        [DataMember]
        public AddressModel Address { get; set; }
        [DataMember]
        public int RateTypeId  { get; set; }
        [DataMember]
        public double AmountDue {get;set;}
        [DataMember]
        public MembershipTypeModel MembershipType { get; set; }
        public void Validate()
        {
            Address.Validate();
            if (MembershipType != null)
            {
                MembershipType.Validate();
            }
            if (RegistrationId < 0)
            {
                throw new RegistrationInvalidException("RegistrationId is required or must be 0");
            }
            if (RateTypeId < 1 || string.IsNullOrEmpty(Year) || EventId < 1 || FamilyId < 1 || FamilyMemberId < 1)
            {
                throw new RegistrationInvalidException("Year, RateTypeId, EventId, FamilyId, FamilyMemberId are required");
            }
            if (AmountDue < 0)
            {
                throw new RegistrationInvalidException("Amount Due must be a positive whole number");
            }
            if (BirthDate >= DateTime.Today)
            {
                throw new RegistrationInvalidException("Birthday Is Invalid");
            }
        }
    }
}