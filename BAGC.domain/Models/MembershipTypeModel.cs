﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using BAGC.domain.Exceptions;

namespace BAGC.domain.Models
{
    [DataContract]
    public class MembershipTypeModel : IValidate
    {
        [DataMember]
        public int MembershipTypeId { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public int Year { get; set; }

        public void Validate()
        {
            if (MembershipTypeId  > 0 && string.IsNullOrEmpty(Name))
            {
                 throw new RegistrationInvalidException("Name of membership must not be null");
            }
            if (MembershipTypeId < 0)
            {
                throw new RegistrationInvalidException("membershiptype id must not be null");
            }
            if (Year <= 0)
            {
                throw new RegistrationInvalidException("Year is not valid for MembershipType");
            }
        }
    }
}
