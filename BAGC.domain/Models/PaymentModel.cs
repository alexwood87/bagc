﻿using System;
using System.Runtime.Serialization;
using BAGC.domain.Exceptions;

namespace BAGC.domain.Models
{
    [DataContract]
    public class PaymentModel : IValidate
    {
        [DataMember]
        public int PaymentId { get; set; }

        [DataMember]
        public string Year { get; set; }

        [DataMember]
        public int FamilyId { get; set; }

        [DataMember]
        public int EventId { get; set; }

        [DataMember]
        public decimal RegistrationAmountDue { get; set; }

        [DataMember]
        public decimal AmountPaid { get; set; }

        [DataMember]
        public int PaymentMethod { get; set; }

        [DataMember]
        public string PaymentIdentifier { get; set; }

        [DataMember]
        public DateTime PaymentDate { get; set; }

        [DataMember]
        public string Comments { get; set; }
        [DataMember]
        public int RateTypeId { get; set; }
        [DataMember]
        public decimal FamilyMembershipDue { get; set; }
        public void Validate()
        {
            if (AmountPaid < 0)
            {
                throw new PaymentInvalidException("Amount Paid Cannot Be Negative");
            }
            if (PaymentDate == DateTime.MinValue || PaymentDate > DateTime.UtcNow.AddDays(1))
            {
                throw new PaymentInvalidException("Payment Date Is Not Valid");
            }
            if (string.IsNullOrEmpty(PaymentIdentifier))
            {
                throw new PaymentInvalidException("Payment Identifier Is Required");
            }
            if (RegistrationAmountDue < 0)
            {
                throw new PaymentInvalidException("Registration Amount Must Be Positive");
            }
            if (string.IsNullOrEmpty(Year) || FamilyId <= 0 || EventId <= 0 || RateTypeId <= 0)
            {
                throw new PaymentInvalidException("FK Violation");
            }
        }
    }
}

