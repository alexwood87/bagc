﻿using System;
using System.IO;
using System.Runtime.Serialization;

namespace BAGC.domain.Models
{
    [DataContract]
    public class RateModel : IValidate
    {
        [DataMember]
        public string Year { get; set; }
        [DataMember]
        public int RateType { get; set; }
        [DataMember]
        public int EventId { get; set; }
        [DataMember]
        public bool IsChild { get; set; }
        [DataMember]
        public DateTime DateCreated { get; set; }
        [DataMember]
        public DateTime DateUpdated { get; set; }
        [DataMember]
        public string Comments { get; set; }
        [DataMember]
        public decimal Amount { get; set; }
        [DataMember]
        public decimal Discount { get; set; }
        [DataMember]
        public MembershipTypeModel MembershipType { get; set; }
        [DataMember]
        public int? MembershipTypeId { get; set; }

        public void Validate()
        {
            if (MembershipTypeId.HasValue && MembershipType != null)
            {
                MembershipType.Validate();
            }
            if (EventId < 0 || RateType < 0 || string.IsNullOrEmpty(Year))
            {
                throw new InvalidDataException("Foreign Key Relationship is violated because a foreign key is negative");
            }
            if (Amount < 0 || Discount < 0)
            {
                throw new InvalidDataException("Amount and Discount must be greater than or equal to 0");
            }
        }
    }
}
