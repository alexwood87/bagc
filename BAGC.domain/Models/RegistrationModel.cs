﻿using System;
using System.Runtime.Serialization;
using BAGC.domain.Exceptions;

namespace BAGC.domain.Models
{
    [DataContract]
    public class RegistrationModel : IValidate
    {
        [DataMember]
        public AddressModel Address { get; set; }
        [DataMember]
        public string Year { get; set; }
        [DataMember]
        public int FamilyId { get; set; }
        [DataMember]
        public int RegistrationId { get; set; }
        [DataMember]
        public int FamilyMemberId { get; set; }
        [DataMember]
        public PaymentModel Payment { get; set; }
        [DataMember]
        public bool Attending { get; set; }
        [DataMember]
        public DateTime DateCreated { get; set; }
        [DataMember]
        public DateTime DateUpdated { get; set; }
        [DataMember]

        public string Comments { get; set; }
        [DataMember]
        public int EventId { get; set; }
        [DataMember]
        public bool Prepaid { get; set; }
        public void Validate()
        {
            Payment.Validate();
            Address.Validate();
            if (RegistrationId < 0)
            {
                throw new RegistrationInvalidException("Registration Id PK Violation");
            }
            if (string.IsNullOrEmpty(Year) || FamilyId <= 0 || FamilyMemberId <= 0 || EventId <= 0)
            {
                throw  new RegistrationInvalidException("Foreign Key Error");
            }
        }
    }
}
