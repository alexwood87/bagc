﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using BAGC.domain.Models.SerializedLinq;

namespace BAGC.domain.Models
{
    [DataContract]
    public class SearchModel
    {
        [DataMember]
        public int PageSize { get; set; }
        [DataMember]
        public int PageIndex { get; set; }
        [DataMember]
        public string SortField { get; set; }
        [DataMember]
        public bool SortAscending { get; set; }
        [DataMember]
        public SerializedLinqQuery Query { get; set; }
    }
}
