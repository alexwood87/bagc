﻿using System.Runtime.Serialization;

namespace BAGC.domain.Models.SerializedLinq
{
    [DataContract]
    public class CompositeExpression : Expression
    {
        [DataMember]
        public ConstantExpression LeftExpression { get; set; }
        [DataMember]
        public ConstantExpression RightExpression { get; set; }
        [DataMember]
        public LogicOperator LogicOperator { get; set; }
    }
}
