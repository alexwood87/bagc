﻿using System.Runtime.Serialization;

namespace BAGC.domain.Models.SerializedLinq
{
    [DataContract]
    public enum ConditionalOperator
    {
        [EnumMember]
        GreaterThan,
        [EnumMember]
        GreaterThanEqual,
        [EnumMember]
        LessThan,
        [EnumMember]
        LessThanEqual,
        [EnumMember]
        Equal,
        [EnumMember]
        NotEqual,
        [EnumMember]
        Contains
    }
}
