﻿using System.Runtime.Serialization;

namespace BAGC.domain.Models.SerializedLinq
{
    [DataContract]
    public class ConstantExpression : Expression
    {
        [DataMember]
        public string PropertyName { get; set; }
        [DataMember]
        public string Value { get; set; }
        [DataMember]
        public SerializedType Type { get; set; }
        [DataMember]
        public ConditionalOperator ConditionalOperator { get; set; }
    }
}
