﻿using System.Runtime.Serialization;

namespace BAGC.domain.Models.SerializedLinq
{
    [DataContract]
    public enum LogicOperator
    {
        [EnumMember]
        AND,
        [EnumMember]
        OR,
        [EnumMember]
        NOT
    }
}
