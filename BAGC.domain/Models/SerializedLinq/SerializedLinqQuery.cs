﻿using System.Runtime.Serialization;

namespace BAGC.domain.Models.SerializedLinq
{
    [DataContract]
    public class SerializedLinqQuery
    {
        [DataMember]
        public CompositeExpression CompositeExpression { get; set; }
        [DataMember]
        public ConstantExpression ConstantExpression { get; set; } 
        [DataMember]
        public int PageSize { get; set; }
        [DataMember]
        public int PageIndex { get; set; }
        [DataMember]
        public string SortProperty { get; set; }
        [DataMember]
        public bool SortAscending { get; set; }
    }
}
