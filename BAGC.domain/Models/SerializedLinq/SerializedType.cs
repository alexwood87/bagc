﻿using System.Runtime.Serialization;

namespace BAGC.domain.Models.SerializedLinq
{
    [DataContract]
    public enum SerializedType
    {
        [EnumMember]
        Int32,
        [EnumMember]
        Double,
        [EnumMember]
        Decimal,
        [EnumMember]
        String,
        [EnumMember]
        Int32Null,
        [EnumMember]
        DoubleNull,
        [EnumMember]
        DecimalNull,
        [EnumMember]
        Long,
        [EnumMember]
        LongNull,
        [EnumMember]
        Float,
        [EnumMember]
        FloatNull,
        [EnumMember]
        DateTime,
        [EnumMember]
        DateTimeNull
    }
}
