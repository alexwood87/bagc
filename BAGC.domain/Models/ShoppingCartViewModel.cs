﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using BAGC.domain.Entities;

namespace BAGC.domain.Models
{
    public class ShoppingCartViewModel
    {
        [Key]
        public int ID { get; set; }
        public int FamilyID { get; set; }
        public string Year { get; set; }
        public int Event { get; set; }

        public List<Cart> CartItems { get; set; }
        public decimal CartTotal { get; set; }
    }
}
