﻿using BAGC.domain.Entities;

namespace BAGC.domain.Repositories
{
    public interface ICartRepository : IGenericRepository<Cart>
    {
        void Delete(Cart cart);
    }
}
