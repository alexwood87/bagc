﻿using System.Collections.Generic;
using BAGC.domain.Entities;
using BAGC.domain.Models;

namespace BAGC.domain.Repositories
{
    public interface IEventRepository : IGenericRepository<Event>
    {
        List<Event> SearchByDescription(string desc);
      
    }
}
