﻿using BAGC.domain.Entities;

namespace BAGC.domain.Repositories
{
    public interface IFamilyMemberRepository : IGenericRepository<FamilyMember>
    {
    }
}
