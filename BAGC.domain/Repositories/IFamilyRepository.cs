﻿using System.Collections.Generic;
using BAGC.domain.Entities;
using BAGC.domain.Models.SerializedLinq;

namespace BAGC.domain.Repositories
{
    public interface IFamilyRepository : IGenericRepository<Family>
    {
        List<Family> Search(SerializedLinqQuery query);

        Family AddFamily(Family family);
    }
}
