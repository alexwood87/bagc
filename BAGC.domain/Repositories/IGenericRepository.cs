﻿using System.Collections.Generic;
using BAGC.domain.Models.SerializedLinq;

namespace BAGC.domain.Repositories
{
    public interface IGenericRepository<TE>
    {
        object Find(object id);
        void Save(TE entity);
        void Update(TE entity);
        TE Add(TE entity);
        List<TE> GetAll();
        List<TE> GetByFunction(SerializedLinqQuery query);
    }
}
