﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BAGC.domain.Entities;

namespace BAGC.domain.Repositories
{
    public interface IMembershipTypeRepository : IGenericRepository<MembershipType>
    {
        MembershipType FindLastestForFamily(int familyId);
        MembershipType FindLatestOverrideForFamilyMember(int familyMemberId);
        MembershipType FindLastestForRateType(int rateTypeId);
        void AddUpdateRateMembershipType(int rateType, int membershipTypeId, int year);
        void AddUpdateFamilyMemberMembershipType(int familyMemberId, int memberhsipTypeId, int? eventId, int year);
        void AddUpdateFamilyMembershipType(int familyId, int memberhsipTypeId, int year);
        void DeleteRateMembershipType(int rateType, int membershipTypeId, int year);
        void DeleteFamilyMemberMembershipType(int familyMemberId, int memberhsipTypeId, int year);
        void DeleteFamilyMembershipType(int familyId, int memberhsipTypeId, int year);

    }
}
