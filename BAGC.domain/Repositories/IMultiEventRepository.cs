﻿using BAGC.domain.Entities;

namespace BAGC.domain.Repositories
{
    public interface IMultiEventRepository : IGenericRepository<Multi_Event>
    {
    }
}
