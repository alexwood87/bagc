﻿using BAGC.domain.Entities;

namespace BAGC.domain.Repositories
{
    public interface IPaymentRepository : IGenericRepository<Payment>
    {
        void Delete(int paymentId);
    }
}
