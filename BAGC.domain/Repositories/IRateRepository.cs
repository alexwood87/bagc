﻿using BAGC.domain.Entities;

namespace BAGC.domain.Repositories
{
    public interface IRateRepository : IGenericRepository<Rate>
    {
    }
}
