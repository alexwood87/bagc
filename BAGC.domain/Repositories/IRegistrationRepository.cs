﻿using BAGC.domain.Entities;

namespace BAGC.domain.Repositories
{
    public interface IRegistrationRepository : IGenericRepository<Registration>
    {
        void Delete(int registrationId);
    }
}
