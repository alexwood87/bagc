﻿using BAGC.domain.Repositories;
namespace BAGC.domain.UnitOfWorks
{
    public interface IUnitOfWork
    {
        IEventRepository EventRepository { get; set; }
        IFamilyMemberRepository FamilyMemberRepository { get; set; }
        IFamilyRepository FamilyRepository { get; set; }
        IMultiEventRepository MultiEventRepository { get; set; }
        ICartRepository CartRepository { get; set; }
        IRateRepository RateRepository { get; set; }
        IRegistrationRepository RegistrationRepository { get; set; }
        IPaymentRepository PaymentRepository { get; set; }
        IMembershipTypeRepository MembershipTypeRepository { get; set; }
        void Save();

    }
}
