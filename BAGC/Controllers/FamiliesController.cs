﻿using System.Web.Mvc;

namespace BAGC.Controllers
{
    public class FamiliesController : Controller
    {
        //private BAGCEntities db = new BAGCEntities();

        //// GET: Families
        //public ActionResult Index()
        //{
        //    return View(db.Families.ToList());
        //}

        //// GET: Families/Details/5
        //public ActionResult Details(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    var family = db.Families.Find(id);
        //    if (family == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(family);
        //}

        //// GET: Families/Create
        //public ActionResult Create()
        //{
        //    return View();
        //}

        //// POST: Families/Create
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include = "ID,FamilyID,MemberTypeID,LastName,FirstName,Address,City,State,Zip,Phone,EMail")] Family family)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Families.Add(family);
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }

        //    return View(family);
        //}

        //// GET: Families/Edit/5
        //public ActionResult Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    var family = db.Families.Find(id);
        //    if (family == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(family);
        //}

        //// POST: Families/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit([Bind(Include = "ID,FamilyID,MemberTypeID,LastName,FirstName,Address,City,State,Zip,Phone,EMail")] Family family)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(family).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    return View(family);
        //}

        //// GET: Families/Delete/5
        //public ActionResult Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    var family = db.Families.Find(id);
        //    if (family == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(family);
        //}

        //// POST: Families/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    var family = db.Families.Find(id);
        //    db.Families.Remove(family);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        //public ActionResult Browse(Int32 Id)
        //{
        //    // Retrieve Genre and its Associated Albums from database
        //    var familyModel = db.Families.Include("FamilyMembers")
        //    .Single(g => g.FamilyID == Id);
        //    return View(familyModel);
        //}
         

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}
