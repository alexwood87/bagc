﻿using System;
using System.Data.Entity;
using System.Net;
using System.Web.Mvc;

namespace BAGC.Controllers
{
    public class FamilyMembersController : Controller
    {
        //private BAGCEntities db = new BAGCEntities();

        //// GET: FamilyMembers
        //public ActionResult Index()
        //{
        //    return View(db.FamilyMembers.ToList());
        //}

        //// GET: FamilyMembers/Details/5
        //public ActionResult Details(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    var familyMember = db.FamilyMembers.Find(id);
        //    if (familyMember == null)
        //    {
        //        return HttpNotFound();
        //    }

        //    return View(familyMember);
        //}

        //// GET: FamilyMembers/Create
        //public ActionResult Create()
        //{
        //    return View();
        //}

        //// POST: FamilyMembers/Create
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include = "FamilyMemberID,FamilyID, LastName,FirstName, Address1,Address2,Address3,City,Sate,Zip,Phone,EMail")] FamilyMember familyMember)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.FamilyMembers.Add(familyMember);
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }

        //    return View(familyMember);
        //}

        //// GET: FamilyMembers/Edit/5
        //public ActionResult Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    var familyMember = db.FamilyMembers.Find(id);
        //    if (familyMember == null)
        //    {
        //        return HttpNotFound();
        //    }

        //    // Drop down

        //    ViewBag.RateList = new SelectList(db.Rates, "RateTypeID", "Amount" ,"Select RateType"  );
            
        //    ViewBag.EventList = new SelectList(db.Events, "EventID", "EventDesc");
             
        //    var famMemberRegister = new FamilyMemberRegistration();

        //    famMemberRegister.FamilyID = familyMember.FamilyID;
        //    famMemberRegister.FamilyMemberID = familyMember.FamilyMemberID;
        //    famMemberRegister.LastName = familyMember.LastName;
        //    famMemberRegister.FirstName = familyMember.FirstName;
        //    famMemberRegister.Address1 = familyMember.Address1;
        //   // famMemberRegister.RateTypeID = new SelectList(db.Rates, "ID", "Amount");
           
        //    famMemberRegister.Year = DateTime.Now.Year.ToString();

        //    return View(famMemberRegister);
        //}

        //// POST: FamilyMembers/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit([Bind(Include = "ID,FamilyMemberID,FamilyID,LastName,FirstName,BirthDate,Address1,Address2,Address3,City,Sate,Zip,Phone,EMail,RateList")] FamilyMemberRegistration famMemberRegister, FormCollection form  )
        //{
            
        //    famMemberRegister.RateTypeID  = Convert.ToInt16(form["RateList"].ToString());
        //    famMemberRegister.EventID = Convert.ToInt16(form["EventList"].ToString());
                     
        //    //famMemberRegister.Event = form["Event"].ToString();
        //    famMemberRegister.Year = form["Year"].ToString();

        //    famMemberRegister.RateTypeID = Convert.ToInt32(form["RateList"]);

        //    famMemberRegister.AmountDue = Convert.ToDouble(form["AmountDue"].ToString());

        //    return RedirectToAction("AddToCart", "ShoppingCart", famMemberRegister);
          
        //    // *************  NOT USED NOW *************
        //    if (ModelState.IsValid)
        //    {
        //        try
        //        {
        //            db.Entry(famMemberRegister).State = EntityState.Modified;
        //            db.SaveChanges();
        //        }
        //        catch (Exception e)
        //        {
        //            //foreach (var eve in e.InnerException)
        //            //{
        //                //Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
        //                //    eve.Entry.Entity.GetType().Name, eve.Entry.State);
        //                //foreach (var ve in eve.ValidationErrors)
        //                //{
        //                //    Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
        //                //        ve.PropertyName, ve.ErrorMessage);
        //                //}
        //            //}
        //            throw;

        //        }               
        //        return RedirectToAction("Index");
        //    }

        //    return View(famMemberRegister);
        //}
         
        //// GET: FamilyMembers/Delete/5
        //public ActionResult Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    var familyMember = db.FamilyMembers.Find(id);
        //    if (familyMember == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(familyMember);
        //}

        //// POST: FamilyMembers/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    var familyMember = db.FamilyMembers.Find(id);
        //    db.FamilyMembers.Remove(familyMember);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}
