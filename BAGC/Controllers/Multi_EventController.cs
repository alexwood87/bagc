﻿using System.Data.Entity;
using System.Net;
using System.Web.Mvc;

namespace BAGC.Controllers
{
    public class Multi_EventController : Controller
    {
        private BAGCEntities db = new BAGCEntities();

        // GET: Multi_Event
        public ActionResult Index()
        {
            return View(db.Multi_Event.ToList());
        }

        // GET: Multi_Event/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var multi_Event = db.Multi_Event.Find(id);
            if (multi_Event == null)
            {
                return HttpNotFound();
            }
            return View(multi_Event);
        }

        // GET: Multi_Event/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Multi_Event/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,MultiEventID,EventID,MultiEventDesc")] Multi_Event multi_Event)
        {
            if (ModelState.IsValid)
            {
                db.Multi_Event.Add(multi_Event);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(multi_Event);
        }

        // GET: Multi_Event/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var multi_Event = db.Multi_Event.Find(id);
            if (multi_Event == null)
            {
                return HttpNotFound();
            }
            return View(multi_Event);
        }

        // POST: Multi_Event/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,MultiEventID,EventID,MultiEventDesc")] Multi_Event multi_Event)
        {
            if (ModelState.IsValid)
            {
                db.Entry(multi_Event).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(multi_Event);
        }

        // GET: Multi_Event/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var multi_Event = db.Multi_Event.Find(id);
            if (multi_Event == null)
            {
                return HttpNotFound();
            }
            return View(multi_Event);
        }

        // POST: Multi_Event/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var multi_Event = db.Multi_Event.Find(id);
            db.Multi_Event.Remove(multi_Event);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
