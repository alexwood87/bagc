﻿using System.Data.Entity;
using System.Net;
using System.Web.Mvc;

namespace BAGC.Controllers
{
    public class Rate_TypeController : Controller
    {
        //private BAGCEntities db = new BAGCEntities();

        //// GET: Rate_Type
        //public ActionResult Index()
        //{
        //    return View(db.Rate_Type.ToList());
        //}

        //// GET: Rate_Type/Details/5
        //public ActionResult Details(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    var rate_Type = db.Rate_Type.Find(id);
        //    if (rate_Type == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(rate_Type);
        //}

        //// GET: Rate_Type/Create
        //public ActionResult Create()
        //{
        //    return View();
        //}

        //// POST: Rate_Type/Create
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include = "ID,RateTypeID,RateTypeDesc,Child")] Rate_Type rate_Type)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Rate_Type.Add(rate_Type);
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }

        //    return View(rate_Type);
        //}

        //// GET: Rate_Type/Edit/5
        //public ActionResult Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    var rate_Type = db.Rate_Type.Find(id);
        //    if (rate_Type == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(rate_Type);
        //}

        //// POST: Rate_Type/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit([Bind(Include = "ID,RateTypeID,RateTypeDesc,Child")] Rate_Type rate_Type)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(rate_Type).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    return View(rate_Type);
        //}

        //// GET: Rate_Type/Delete/5
        //public ActionResult Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    var rate_Type = db.Rate_Type.Find(id);
        //    if (rate_Type == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(rate_Type);
        //}

        //// POST: Rate_Type/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    var rate_Type = db.Rate_Type.Find(id);
        //    db.Rate_Type.Remove(rate_Type);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}
