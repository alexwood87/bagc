﻿using System.Web.Mvc;
using BAGC.Models;
using BAGC.Proxy.BagcMainService;

namespace BAGC.Controllers
{
    public class ShoppingCartController : Controller
    {
        //private BAGCEntities storeDB = new BAGCEntities();

        ////
        //// GET: /ShoppingCart/

        //public ActionResult Index(FamilyMemberRegistration famMemReg)
        //{
        //    var cart = ShoppingCart.GetCart(this.HttpContext);

        //    // Set up our ViewModel
        //    var viewModel = new ShoppingCartViewModel
        //    {
        //        FamilyID = famMemReg.FamilyID,
        //        Event  =  famMemReg.EventID,
        //        Year = famMemReg.Year,
        //        CartItems = cart.GetCartItems()
        //       //  CartTotal = cart.GetTotal()
        //    };

        //    // Return the view
        //    return View(viewModel);
        //}

        ////
        //// GET: /Store/AddToCart/5

        //public ActionResult AddToCart1(int id)
        //{

        //    // Retrieve the FamilyMember from the database
        //    var addedFamMember = storeDB.FamilyMembers
        //        .Single(member => member.FamilyMemberID == id);

        //    // Add it to the shopping cart
        //    var cart = ShoppingCart.GetCart(this.HttpContext);

        // //   cart.AddToCart( .AddToCart(addedFamMember);

        //    // Go back to the main store page for more shopping
        //    return RedirectToAction("Index");
        //}

        //public ActionResult AddToCart(FamilyMemberRegistration famMemReg)
        //{

        //    // Retrieve the FamilyMember from the database
        //    //var addedFamMember = storeDB.FamilyMembers
        //    //    .Single(member => member.FamilyMemberID == id);

        //    //*** STEP #1    Basically getting an id for the shopping cart
        //    var shopCart = ShoppingCart.GetCart(this.HttpContext);  

             
        //    //*** STEP #2    Add this instance of famMemReg to the shopping cart
        //    shopCart.AddToCart(famMemReg);

        //    // Go back to the main store page for more shopping
        //    return RedirectToAction("Index", famMemReg);
        //}

        //
        // AJAX: /ShoppingCart/RemoveFromCart/5

      //  [HttpPost]
        //public ActionResult RemoveFromCart(int id)
        //{
        //    // Remove the item from the cart
        //    var cart = ShoppingCart.GetCart(this.HttpContext);

        //    // Get the name of the album to display confirmation

        //    string albumName = "Family Member ";
        //    //string albumName = storeDB.Carts
        //    //    .Single(item => item.RecordId == id).Album.Title;

        //    // Remove from cart
        ////    int itemCount = cart.RemoveFromCart(id);

        //    // Display the confirmation message
        //    var results = new ShoppingCartRemoveViewModel
        //    {
        //        Message = Server.HtmlEncode(albumName) +
        //            " has been removed from your shopping cart.",
        //        CartTotal = cart.GetTotal(),
        //        CartCount = cart.GetCount(),
        //        ItemCount = itemCount,
        //        DeleteId = id
        //    };

        //    return Json(results);
        //}

        //
        // GET: /ShoppingCart/CartSummary

     //   [ChildActionOnly]
        //public ActionResult CartSummary()
        //{
        //    var cart = ShoppingCart.GetCart(this.HttpContext);

        //    ViewData["CartCount"] = cart.GetCount();

        //    return PartialView("CartSummary");
        //}
    }
}