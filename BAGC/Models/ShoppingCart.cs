﻿using System;
using System.Web;
using System.Web.Mvc;
using BAGC.Proxy.BagcMainService;

namespace BAGC.Models
{
    public partial class ShoppingCart
    {

        string ShoppingCartId { get; set; }

        public const string CartSessionKey = "CartId";

        public static ShoppingCart GetCart(HttpContextBase context)
        {
            var shopCart = new ShoppingCart();
            shopCart.ShoppingCartId = shopCart.GetCartId(context);             
            return shopCart;
        }

        // We're using HttpContextBase to allow access to cookies.
        public string GetCartId(HttpContextBase context)
        {
            if (context.Session[CartSessionKey] == null)
            {
                if (!string.IsNullOrWhiteSpace(context.User.Identity.Name))
                {
                    context.Session[CartSessionKey] = context.User.Identity.Name;
                }
                else
                {
                    // Generate a new random GUID using System.Guid class
                    var tempCartId = Guid.NewGuid();

                    // Send tempCartId back to client as a cookie
                    context.Session[CartSessionKey] = tempCartId.ToString();
                }
            }

            return context.Session[CartSessionKey].ToString();
        }


        // Helper method to simplify shopping cart calls
        public static ShoppingCart GetCart(Controller controller)
        {
            return GetCart(controller.HttpContext);
        }

        public void AddToCart(FamilyMemberRegistration FamMemReg)
        {
            //// Get the matching cart and album instances
            //var cartItem = storeDB.Carts.SingleOrDefault(
            //                        c => c.FamilyID == FamMemReg.FamilyID
            //                        && c.FamilyMemberID == FamMemReg.FamilyMemberID
            //                        && c.EventID == FamMemReg.EventID
            //                        && c.Year == FamMemReg.Year);


            //if (cartItem == null)
            //{
            //    // Create a new cart item if no cart item exists
            //    cartItem = new Cart
            //    {
            //        FamilyID = FamMemReg.FamilyID,
            //        FamilyMemberID = FamMemReg.FamilyMemberID,
            //        RateTypeID = FamMemReg.RateTypeID,
            //        EventID = FamMemReg.EventID,
            //        Year = FamMemReg.Year,
            //        AmountDue = Convert.ToDecimal(FamMemReg.AmountDue),
            //        DateCreated = DateTime.Now,
            //        LastName = FamMemReg.LastName,
            //        FirstName = FamMemReg.FirstName
            //    };

            //    storeDB.Carts.Add(cartItem);
            //}
            //else
            //{
            //    // If the item does exist in the cart, then add one to the quantity
            //    cartItem.AmountDue =   Convert.ToDecimal(FamMemReg.AmountDue);
            //}

            //// Save changes
            //try
            //{
            //    storeDB.SaveChanges();
            //}

            //catch (DbEntityValidationException e)
            //{
            //    //foreach (var eve in e.InnerException)
            //    //{
            //    //    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
            //    //        eve.Entry.Entity.GetType().Name, eve.Entry.State);
            //    //    foreach (var ve in eve.ValidationErrors)
            //    //    {
            //    //        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
            //    //            ve.PropertyName, ve.ErrorMessage);
            //    //    }
            //    //}
            //    //throw;
            //}
        }


        //public void AddToCart(FamilyMember FamilyMember)
        //{
        //    // Get the matching cart and album instances
        //    var cartItem = storeDB.Carts.SingleOrDefault(
        //                            c => c.CartID == ShoppingCartId
        //                            && c.FamilyMemberID == FamilyMember.FamilyMemberID);

        //    if (cartItem == null)
        //    {
        //        // Create a new cart item if no cart item exists
        //        cartItem = new Cart
        //        {
        //            FamilyMemberID = FamilyMember.FamilyMemberID,
        //            CartID = ShoppingCartId,
        //            Count = 1,
        //            DateCreated = DateTime.Now
        //        };

        //        storeDB.Carts.Add(cartItem);
        //    }
        //    else
        //    {
        //        // If the item does exist in the cart, then add one to the quantity
        //        cartItem.Count++;
        //    }

        //    // Save changes
        //    storeDB.SaveChanges();
        //}

        //public void AddToCart(FamilyMemberRegistration FamMemReg)
        //{
        //    // Get the matching cart   items
        //    var cartItem = storeDB.Registrations.SingleOrDefault(
        //                            p => p.FamilyID == FamMemReg.FamilyID
                                    
        //                            && p.EventID == FamMemReg.EventID 
        //                            && p.Year == FamMemReg.Year);
                                    

        //    if (cartItem == null)
        //    {
        //        // Create a new cart item if no cart item exists
        //        cartItem = new Registration
        //        {
        //             FamilyID = FamMemReg.FamilyID,
                    
        //              RateTypeID =   FamMemReg.RateTypeID ,  
        //              EventID = FamMemReg.EventID,
        //             Year = FamMemReg.Year,
        //             AmountDue =  Convert.ToDecimal(FamMemReg.AmountDue),
        //             DateCreated = DateTime.Now,
        //             LastName = FamMemReg.LastName, 
        //             FirstName = FamMemReg.FirstName
        //        };

        //        storeDB.Registrations.Add(cartItem);
        //    }
        //    else
        //    {
        //        // If the item does exist in the cart, then add one to the quantity
        //       // cartItem.Count++;
        //    }

        //    // Save changes
        //    try
        //    {
        //        storeDB.SaveChanges();
        //    }

        //    catch (DbEntityValidationException e)
        //    {
        //        //foreach (var eve in e.InnerException)
        //        //{
        //        //    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
        //        //        eve.Entry.Entity.GetType().Name, eve.Entry.State);
        //        //    foreach (var ve in eve.ValidationErrors)
        //        //    {
        //        //        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
        //        //            ve.PropertyName, ve.ErrorMessage);
        //        //    }
        //        //}
        //        //throw;
        //    }
        //}

        //public int RemoveFromCart(int id)
        //{
        //    // Get the cart
        //    var cartItem = storeDB.Carts.Single(
        //                    cart => cart.CartID == ShoppingCartId
        //                    && cart.CartID == id.ToString());

        //    int itemCount = 0;

        //    if (cartItem != null)
        //    {
        //        if (cartItem.Count > 1)
        //        {
        //            cartItem.Count--;
        //            itemCount = cartItem.Count;
        //        }
        //        else
        //        {
        //            storeDB.Carts.Remove(cartItem);
        //        }

        //        // Save changes
        //        storeDB.SaveChanges();
        //    }

        //    return itemCount;
        //}

        //public void EmptyCart()
        //{
        //    var cartItems = storeDB.Carts.Where(cart => cart.CartID == ShoppingCartId);

        //    foreach (var cartItem in cartItems)
        //    {
        //        storeDB.Carts.Remove(cartItem);
        //    }

        //    // Save changes
        //    storeDB.SaveChanges();
        //}

        //public List<Cart> GetCartItems()
        //{
            //return storeDB.Carts.Where(cart => cart.CartID == ShoppingCartId).ToList();
            //return storeDB.Carts.ToList();
        //}

        //public int GetCount()
        //{
        //    // Get the count of each item in the cart and sum them up
        //    int? count = (from cartItems in storeDB.Carts
        //                  where cartItems.CartID == ShoppingCartId
        //                  select (int?)cartItems.Count).Sum();

        //    // Return 0 if all entries are null
        //    return count ?? 0;
        //}

        //public decimal GetTotal()
        //{
        //    // Multiply album price by count of that album to get 
        //    // the current price for each of those albums in the cart
        //    // sum all album price totals to get the cart total
        //    decimal? total = (from cartItems in storeDB.Carts
        //                      where cartItems.CartID == ShoppingCartId
        //                      select (int?)cartItems.Count * cartItems.FamilyMemberID).Sum();
        //    return total ?? decimal.Zero;
        //}

        //public int CreateOrder(Order order)
        //{
        //    decimal orderTotal = 0;

        //    var cartItems = GetCartItems();

        //    // Iterate over the items in the cart, adding the order details for each
        //    foreach (var item in cartItems)
        //    {
        //        var orderDetail = new OrderDetail
        //        {
        //            FamilyMemberID = item.FamilyMemberID,
        //            OrderId = order.OrderId,
        //          //  UnitPrice = item.Album.Price,
        //            Quantity = item.Count
        //        };

        //        // Set the order total of the shopping cart
        //        orderTotal += (item.Count * item.FamilyMemberID);

        //       // storeDB.OrderDetails.Add(orderDetail);

        //    }

        //    // Set the order's total to the orderTotal count
        //    order.Total = orderTotal;

        //    // Save the order
        //    storeDB.SaveChanges();

        //    // Empty the shopping cart
        //    EmptyCart();

        //    // Return the OrderId as the confirmation number
        //    return order.OrderId;
        //}
              
        //// When a user has logged in, migrate their shopping cart to
        //// be associated with their username
        //public void MigrateCart(string userName)
        //{
        //    var shoppingCart = storeDB.Carts.Where(c => c.CartID == ShoppingCartId);

        //    foreach (Cart item in shoppingCart)
        //    {
        //        item.CartID = userName;
        //    }
        //    storeDB.SaveChanges();
        //}
    }
}