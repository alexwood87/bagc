﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BAGC.Models;

namespace BAGC.ViewModels
{
    public class FamilyMemberRegistration
    {
        public string Year { get; set; }
        public int EventID { get; set; }          
        public int FamilyID { get; set; }
        public int FamilyMemberID { get; set; }

        public string LastName { get; set; }
        public string FirstName { get; set; }
        public Nullable<System.DateTime> BirthDate { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string City { get; set; }
        public string Sate { get; set; }
        public string Zip { get; set; }
        public string Phone { get; set; }
        public string EMail { get; set; }

        public int RateTypeID  { get; set; }
        public double AmountDue {get;set;}       
      
        public virtual Family Family { get; set; }
    }
}