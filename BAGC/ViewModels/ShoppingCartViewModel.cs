﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BAGC.Models;
using System.ComponentModel.DataAnnotations;

namespace BAGC.ViewModels
{
    public class ShoppingCartViewModel
    {
        [Key]
        public int ID { get; set; }
        public int FamilyID { get; set; }
        public string Year { get; set; }
        public int Event { get; set; }

        public List<Cart> CartItems { get; set; }
        public decimal CartTotal { get; set; }
    }
}
