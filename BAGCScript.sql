USE [master]
GO
/****** Object:  Database [BAGC]    Script Date: 11/19/2015 9:17:48 PM ******/
CREATE DATABASE [BAGC]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'BAGC', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\BAGC.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'BAGC_log', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\BAGC_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [BAGC] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [BAGC].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [BAGC] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [BAGC] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [BAGC] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [BAGC] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [BAGC] SET ARITHABORT OFF 
GO
ALTER DATABASE [BAGC] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [BAGC] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [BAGC] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [BAGC] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [BAGC] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [BAGC] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [BAGC] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [BAGC] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [BAGC] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [BAGC] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [BAGC] SET  DISABLE_BROKER 
GO
ALTER DATABASE [BAGC] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [BAGC] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [BAGC] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [BAGC] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [BAGC] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [BAGC] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [BAGC] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [BAGC] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [BAGC] SET  MULTI_USER 
GO
ALTER DATABASE [BAGC] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [BAGC] SET DB_CHAINING OFF 
GO
ALTER DATABASE [BAGC] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [BAGC] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [BAGC]
GO
/****** Object:  User [bagcuser]    Script Date: 11/19/2015 9:17:48 PM ******/
CREATE USER [bagcuser] FOR LOGIN [bagcuser] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [bagcuser]
GO
/****** Object:  Table [dbo].[Cart]    Script Date: 11/19/2015 9:17:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cart](
	[CartId] [bigint] IDENTITY(1,1) NOT NULL,
	[Year] [nchar](4) NOT NULL,
	[EventID] [int] NOT NULL,
	[FamilyID] [int] NOT NULL,
	[FamilyMemberID] [int] NOT NULL,
	[LastName] [nchar](20) NOT NULL,
	[FirstName] [nchar](20) NOT NULL,
	[BirthDate] [date] NULL,
	[Address1] [nvarchar](50) NULL,
	[Address2] [nvarchar](50) NULL,
	[Address3] [nvarchar](50) NULL,
	[City] [nvarchar](50) NULL,
	[State] [nchar](10) NULL,
	[Zip] [nchar](10) NULL,
	[Phone] [nchar](15) NULL,
	[EMail] [nchar](50) NULL,
	[RateTypeID] [int] NULL,
	[AmountDue] [decimal](10, 2) NULL,
	[DateCreated] [datetime] NULL,
 CONSTRAINT [PK_Cart] PRIMARY KEY CLUSTERED 
(
	[CartId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Event]    Script Date: 11/19/2015 9:17:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Event](
	[EventID] [int] IDENTITY(1,1) NOT NULL,
	[EventDesc] [nchar](20) NULL,
	[Discount] [bit] NULL,
	[Current] [bit] NULL,
	[MultiEvent] [bit] NULL,
	[EventDate] [datetime] NOT NULL,
 CONSTRAINT [PK_Event] PRIMARY KEY CLUSTERED 
(
	[EventID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Family]    Script Date: 11/19/2015 9:17:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Family](
	[FamilyID] [int] IDENTITY(1,1) NOT NULL,
	[LastName] [nchar](20) NOT NULL,
	[FirstName] [nchar](20) NOT NULL,
	[Address1] [nvarchar](50) NULL,
	[Address2] [nvarchar](50) NULL,
	[Address3] [nvarchar](50) NULL,
	[City] [nvarchar](50) NULL,
	[State] [nchar](10) NULL,
	[Zip] [nchar](10) NULL,
	[Phone] [nchar](15) NULL,
	[EMail] [nchar](50) NULL,
	[DateCreated] [datetime] NULL,
	[DateUpdated] [datetime] NULL,
	[Comment] [nchar](30) NULL,
 CONSTRAINT [PK_Family] PRIMARY KEY CLUSTERED 
(
	[FamilyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FamilyMember]    Script Date: 11/19/2015 9:17:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FamilyMember](
	[FamilyMemberID] [int] IDENTITY(1,1) NOT NULL,
	[FamilyID] [int] NOT NULL,
	[LastName] [nchar](20) NOT NULL,
	[FirstName] [nchar](20) NOT NULL,
	[Address1] [nvarchar](50) NULL,
	[Address2] [nvarchar](50) NULL,
	[Address3] [nvarchar](50) NULL,
	[City] [nvarchar](50) NULL,
	[State] [nchar](10) NULL,
	[Zip] [nchar](10) NULL,
	[Phone] [nchar](15) NULL,
	[EMail] [nchar](50) NULL,
	[DateCreated] [datetime] NULL,
	[DateUpdated] [datetime] NULL,
	[Comment] [nchar](30) NULL,
 CONSTRAINT [PK_FamilyMember] PRIMARY KEY CLUSTERED 
(
	[FamilyMemberID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Multi_Event]    Script Date: 11/19/2015 9:17:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Multi_Event](
	[MultiEventID] [int] IDENTITY(1,1) NOT NULL,
	[EventID] [int] NOT NULL,
	[MultiEventDesc] [nchar](30) NULL,
 CONSTRAINT [PK_MultiEvent] PRIMARY KEY CLUSTERED 
(
	[MultiEventID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Payment]    Script Date: 11/19/2015 9:17:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Payment](
	[PaymentID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Year] [nchar](10) NOT NULL,
	[EventID] [int] NOT NULL,
	[FamilyID] [int] NOT NULL,
	[FamilyMembershipDue] [decimal](10, 2) NOT NULL,
	[RegistrationAmountDue] [decimal](10, 2) NOT NULL,
	[AmountPaid] [decimal](10, 2) NOT NULL,
	[Balance] [decimal](10, 2) NOT NULL,
	[PaymentMethod] [int] NOT NULL,
	[PaymentIdentifier] [nchar](30) NULL,
	[PaymentDate] [date] NOT NULL,
	[Comment] [nchar](30) NULL,
 CONSTRAINT [PK_Payment] PRIMARY KEY CLUSTERED 
(
	[PaymentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Rate]    Script Date: 11/19/2015 9:17:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Rate](
	[RateTypeID] [int] IDENTITY(1,1) NOT NULL,
	[Year] [nchar](4) NOT NULL,
	[EventID] [int] NOT NULL,
	[Amount] [decimal](10, 2) NULL,
	[DiscountAmount] [decimal](10, 2) NULL,
	[Child] [bit] NULL,
	[DateCreated] [datetime] NULL,
	[DateUpdated] [datetime] NULL,
	[Comment] [nchar](30) NULL,
 CONSTRAINT [PK_Rate] PRIMARY KEY CLUSTERED 
(
	[RateTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Rate_Type]    Script Date: 11/19/2015 9:17:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Rate_Type](
	[RateTypeID] [int] IDENTITY(1,1) NOT NULL,
	[RateTypeDesc] [nchar](30) NULL,
	[Child] [bit] NULL,
 CONSTRAINT [PK_Rate_Type] PRIMARY KEY CLUSTERED 
(
	[RateTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Registration]    Script Date: 11/19/2015 9:17:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Registration](
	[RegistrationID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[PaymentID] [int] NULL,
	[Year] [nchar](4) NOT NULL,
	[EventID] [int] NOT NULL,
	[FamilyID] [int] NOT NULL,
	[FamilyMemberID] [int] NOT NULL,
	[LastName] [nchar](20) NOT NULL,
	[FirstName] [nchar](20) NOT NULL,
	[RateTypeID] [int] NOT NULL,
	[AmountDue] [decimal](10, 2) NULL,
	[Prepaid] [bit] NULL,
	[Attending] [bit] NULL,
	[Address1] [nvarchar](50) NULL,
	[Address2] [nvarchar](50) NULL,
	[Address3] [nvarchar](50) NULL,
	[City] [nvarchar](50) NULL,
	[State] [nchar](10) NULL,
	[Zip] [nchar](10) NULL,
	[Phone] [nvarchar](15) NULL,
	[EMail] [nvarchar](50) NULL,
	[DateCreated] [datetime] NULL,
	[DateUpdated] [datetime] NULL,
	[Comment] [nchar](30) NULL,
 CONSTRAINT [PK_Registration] PRIMARY KEY CLUSTERED 
(
	[RegistrationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[FamilyMember]  WITH CHECK ADD FOREIGN KEY([FamilyID])
REFERENCES [dbo].[Family] ([FamilyID])
GO
ALTER TABLE [dbo].[FamilyMember]  WITH CHECK ADD FOREIGN KEY([FamilyID])
REFERENCES [dbo].[Family] ([FamilyID])
GO
ALTER TABLE [dbo].[Registration]  WITH CHECK ADD FOREIGN KEY([FamilyID])
REFERENCES [dbo].[Family] ([FamilyID])
GO
ALTER TABLE [dbo].[Registration]  WITH CHECK ADD FOREIGN KEY([PaymentID])
REFERENCES [dbo].[Payment] ([PaymentID])
GO
USE [master]
GO
ALTER DATABASE [BAGC] SET  READ_WRITE 
GO
