CREATE TABLE MembershipType(MembershipTypeId int primary key identity(1,1),
Name NVARCHAR(100) not null, [Year] int not null)

CREATE TABLE MembershipTypeToFamily(MembershipTypeToFamily int primary key
identity(1,1), MembershipTypeId int not null, FamilyId int not null,
DateCreated DateTime default(GetDate()),
foreign key(MembershipTypeId) references MembershipType(MembershipTypeId),
foreign key(FamilyId) references Family(FamilyId))

CREATE TABLE RateToMembershipType(RateToMembershipTypeId int primary key identity(1,1),
DateCreated DateTime default(GETDATE()), 
MembershipTypeId int not null, RateTypeId int not null,
foreign key(RateTypeId) references Rate(RateTypeID),
foreign key(MembershipTypeId) references MembershipType(MembershipTypeId))

CREATE TABLE MembershipTypeToFamilyMemberOverride( 
MembershipToFamilyMemberOverrideId int not null primary key identity(1,1),
MembershipTypeId int not null,
FamilyMemberId int not null, EventId int null,
Foreign key(EventId) references Event(EventId),
Foreign key(MembershipTypeId) references MembershipType(MembershipTypeId),
foreign key(FamilyMemberId) references FamilyMember(FamilyMemberId))
